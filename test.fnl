;; scraped from the wiki
(local fns {:BDR {:metadata {:fnl/arglist ["row"]}}
            :BOOT {:metadata {:fnl/arglist {}}}
            :MENU {:metadata {:fnl/arglist ["index"]}}
            :SCN {:metadata {:fnl/arglist ["row"]}}
            :TIC {:metadata {:fnl/arglist {}}}
            :btn {:metadata {:fnl/arglist ["id"]}}
            :btnp {:metadata {:fnl/arglist ["id" "hold" "period"]}}
            :circ {:metadata {:fnl/arglist ["x" "y" "radius" "color"]}}
            :circb {:metadata {:fnl/arglist ["x" "y" "radius" "color"]}}
            :clip {:metadata {:fnl/arglist ["x" "y" "width" "height"]}}
            :cls {:metadata {:fnl/arglist ["color"]}}
            :elli {:metadata {:fnl/arglist ["x" "y" "a" "b" "color"]}}
            :ellib {:metadata {:fnl/arglist ["x" "y" "a" "b" "color"]}}
            :exit {:metadata {:fnl/arglist {}}}
            :fget {:metadata {:fnl/arglist ["sprite_id" "flag"]}}
            :font {:metadata {:fnl/arglist ["text"
                                            "x"
                                            "y"
                                            "chromakey"
                                            "char_width"
                                            "char_height"
                                            "fixed"
                                            "scale"
                                            "alt"]}}
            :fset {:metadata {:fnl/arglist ["sprite_id" "flag" "bool"]}}
            :key {:metadata {:fnl/arglist ["code"]}}
            :keyp {:metadata {:fnl/arglist ["code" "hold" "period"]}}
            :line {:metadata {:fnl/arglist ["x0" "y0" "x1" "y1" "color"]}}
            :map {:metadata {:fnl/arglist ["x"
                                           "y"
                                           "w"
                                           "h"
                                           "sx"
                                           "sy"
                                           "colorkey"
                                           "scale"
                                           "remap"]}}
            :memcpy {:metadata {:fnl/arglist ["dest" "source" "size"]}}
            :memset {:metadata {:fnl/arglist ["dest" "value" "size"]}}
            :mget {:metadata {:fnl/arglist ["x" "y"]}}
            :mouse {:metadata {:fnl/arglist {}}}
            :mset {:metadata {:fnl/arglist ["x" "y" "tile_id"]}}
            :music {:metadata {:fnl/arglist ["track"
                                             "frame"
                                             "row"
                                             "loop"
                                             "sustain"
                                             "tempo"
                                             "speed"]}}
            :peek {:metadata {:fnl/arglist ["addr" "bits"]}}
            :peek1 {:metadata {:fnl/arglist ["addr"]}}
            :peek2 {:metadata {:fnl/arglist ["addr"]}}
            :peek4 {:metadata {:fnl/arglist ["addr"]}}
            :pix {:metadata {:fnl/arglist ["x" "y" "color"]}}
            :pmem {:metadata {:fnl/arglist ["index" "value"]}}
            :poke {:metadata {:fnl/arglist ["addr" "value" "bits"]}}
            :poke1 {:metadata {:fnl/arglist ["addr" "value"]}}
            :poke2 {:metadata {:fnl/arglist ["addr" "value"]}}
            :poke4 {:metadata {:fnl/arglist ["addr" "value"]}}
            :print {:metadata {:fnl/arglist ["text"
                                             "x"
                                             "y"
                                             "color"
                                             "fixed"
                                             "scale"
                                             "smallfont"]}}
            :rect {:metadata {:fnl/arglist ["x" "y" "w" "h" "color"]}}
            :rectb {:metadata {:fnl/arglist ["x" "y" "w" "h" "color"]}}
            :reset {:metadata {:fnl/arglist {}}}
            :sfx {:metadata {:fnl/arglist ["id"
                                           "note"
                                           "duration"
                                           "channel"
                                           "volume"
                                           "speed"]}}
            :spr {:metadata {:fnl/arglist ["id"
                                           "x"
                                           "y"
                                           "colorkey"
                                           "scale"
                                           "flip"
                                           "rotate"
                                           "w"
                                           "h"]}}
            :sync {:metadata {:fnl/arglist ["mask" "bank" "tocart"]}}
            :time {:metadata {:fnl/arglist {}}}
            :trace {:metadata {:fnl/arglist ["message" "color"]}}
            :tri {:metadata {:fnl/arglist ["x1" "y1" "x2" "y2" "x3" "y3" "color"]}}
            :trib {:metadata {:fnl/arglist ["x1" "y1" "x2" "y2" "x3" "y3" "color"]}}
            :tstamp {:metadata {:fnl/arglist {}}}
            :ttri {:metadata {:fnl/arglist ["x1"
                                            "y1"
                                            "x2"
                                            "y2"
                                            "x3"
                                            "y3"
                                            "u1"
                                            "v1"
                                            "u2"
                                            "v2"
                                            "u3"
                                            "v3"
                                            "texsrc"
                                            "chromakey"
                                            "z1"
                                            "z2"
                                            "z3"]}}
            :vbank {:metadata {:fnl/arglist ["bank"]}}})
(local fennel (require :fennel))
(local oldprint print)
(each [k _v (pairs fns)]
  (tset _G k (fn [] nil)))

(let [allowed (icollect [k (pairs _G)] k)]
  (set _G.trace oldprint)
  (fennel.dofile (. arg 1) {:allowedGlobals allowed :correlate true}))

;; fake out the map
(local map {})

(fn in-bounds? [x y] (and (<= 0 x 239) (<= 0 y 135)))
(fn _G.mset [tx ty t]
  (assert (in-bounds? tx ty))
  (when (not (. map ty))
    (tset map ty []))
  (tset map ty tx t))

(fn _G.mget [tx ty]
  (assert (in-bounds? tx ty) (.. "out of bounds: " tx "x" ty))
  (or (?. map ty tx) 0))

(fn _G.mouse [] (values 8 8))

(math.randomseed (os.time))

(for [i 1 599999]
  (when (= i 45000)
    ;; order food built
    (table.insert _G.state.orders [56 1000 24 24
                                   _G.state.blueprints.food]))
  (_G.TIC)
  (when (< 100 _G.state.height)
    (_G.trace (fennel.view _G.state.workers))
    (os.exit)))
