;; title:   The Tower Institute of Linguistics
;; author:  Matthew Lyon and Phil Hagelberg
;; desc:    build a tower
;; site:    https://codeberg.org/technomancy/tower-institute-of-linguistics
;; license: MIT License
;; version: 0.1
;; script:  fennel
;; strict:  true
;; input:   mouse

(local state (or (. _G :state)
                 {:workers [] :languages [1] :clouds []
                  :height 0 :shake 0 :deiciders 0 :to-deicide 8
                  :cx 0 :cy -920 :speed 1 :win-scroll 0
                  :claims {} :orders [] :need-help []
                  :pause false :arrivals {0 true} :shops [] :need-staff {}
                  :dialog nil :last-mouse nil :said nil :to-say []
                  :focused nil :left 0 :hunger-chance 0.9
                  :build-min 48 :build-max 96
                  :thresholds {:eat 256 :study 384 :rest 512 :heal 0
                               :leave-hunger 1024
                               :leave-frustration 1024
                               :leave-fatigue 1500
                               :leave-build 2048
                               :leave-medic 4096
                               :boredom 120}
                  :god {:x 4 :y 204 :sprite 448 :dy 0 :ddy 0}
                  :blueprints {:food [[ 9 176 177 178]
                                      [25 192 193 194]
                                      [25 208 209 210]]
                               :beds [[181 182]
                                      [197 198]
                                      [213 214]]
                               :study [[ 9 179 180 178]
                                       [25 195 196 194]
                                       [25 211 212 210]]
                               :medic [[ 9 183 184 185 186 178]
                                       [25 199 200 201 202 194]
                                       [25 215 216 217 218 210]]}}))

(tset _G :state state)

(local tempo 
  (let [tpb 72
        self {:tic 0
              :entropy 0 :perc 0
              :beat tpb :bar (* tpb 4) :cycle (* tpb 4 24)
              :s16 (/ tpb 4) :t16 (/ tpb 6) :s8 (/ tpb 2) :t8 (/ tpb 3)
              :lcm (/ tpb 12)}]
    (fn self.tick [_]
      (set self.tic (-> self.tic (+ 1) (% self.cycle))))
    self))

(fn print2 [text x y c1 c2]
  (print text (+ x 1) (+ y 1) c2)
  (print text x y c1))

(fn find [t x ?k] (match (next t ?k) (k x) k (k y) (find t x k)))
(fn copy [t] (collect [k v (pairs t)] k v))
(macro update! [t k f ...] `(tset ,t ,k (,f (. ,t ,k) ,...)))
(fn inc [t k] (update! t k + 1))
(fn sorter [f] (fn [a b] (< (f a) (f b))))

(fn at? [a b] (and (= 0 (- a.y b.y) (- a.x b.x))))

(fn distance [{:x x1 :y y1} {:x x2 :y y2}]
  (let [dx (- x1 x2) dy (- y1 y2)]
    (math.sqrt (+ (* dx dx) (* dy dy)))))

(fn last [t] (. t (length t)))
(fn align [n] (* 8 (// n 8)))

(fn randomchoice [t] (. t (math.random (length t))))

(fn dbg [...]
  (trace (table.concat (icollect [_ x (ipairs [...])] (tostring x)) " ")))

(fn iter-rect [t]
  (var (x y) (values 1 1))
  (fn []
    (let [xy [x y]]
      (set x (+ x 1))
      (when (and (. t y) (< (length (. t y)) x))
        (set (y x) (values (+ y 1) 1)))
      (if (. t (. xy 2))
          xy))))

(local tiles {16 :wall 17 :wall 18 :wall 19 :wall
              20 :wall-ceiling 21 :wall-ceiling
              32 :stairs 48 :stairs
              7 :floor 8 :floor
              1 :ground 2 :ground 3 :ground 4 :ground
              5 :ground-surface 6 :ground-surface
              (. state.blueprints.food 2 2) :food
              (. state.blueprints.beds 2 2) :bed
              (. state.blueprints.study 2 2) :study
              (. state.blueprints.medic 2 2) :medic})

(local tile-nums [])

(each [n t (pairs tiles)]
  (if (= :table (type (. tile-nums t)))
      (table.insert (. tile-nums t) n)
      (= :number (type (. tile-nums t)))
      (tset tile-nums t [(. tile-nums t) n])
      (tset tile-nums t n)))

(fn tile-num [tile-type]
  (let [t (. tile-nums tile-type)]
    (if (= :number (type tile-type)) tile-type ; tolerate number input
        (= :number (type t)) t (randomchoice t))))

(fn lang-color [l] (+ l 9))
(fn lang-flag [l] (values (+ l 265) (+ l 281)))

(fn language-overlap? [a b]
  (faccumulate [shared nil ia 1 2 &until shared]
    (faccumulate [shared nil ib 1 2 &until shared]
      (if (= (. a.languages ia) (. b.languages ib)) (. b.languages ib)))))

(local supply-max 8)
(local ground-level (* 8 128))
(local top-level (* 8 29))
(local cloud-level (* 8 25))
(local bottom-level 920)
(local stair-x state.build-min)
(local quarry {:a {:x 2 :y (- ground-level 1)}
               :b {:x 8 :y (- ground-level 1)}
               :queue {:x 16 :y (- ground-level 1)}})
(local farm {:x 250 :y (- ground-level 1)})
(local exit {:x 480 :y (- ground-level 1)})

(local (build-time quarry-time) (values 100 100))

(local clouds [{:sprite 452 :w 2 :h 2}
               {:sprite 485 :w 1 :h 1}
               {:sprite 500 :w 2 :h 1}])

(fn make-cloud [{: sprite : w : h} ?y ?x ?dx]
  {: sprite : w : h :dx (or ?dx (/ (- (math.random) 0.1) 8))
   :x (or ?x (math.random 240)) :y (or ?y (+ cloud-level (math.random 16)))})

(for [_ 1 8] ; top clouds
  (table.insert state.clouds (make-cloud (randomchoice clouds))))
(for [y (- ground-level 200) (+ cloud-level 100) -120] ; scattered clouds
  (table.insert state.clouds (make-cloud (randomchoice clouds) y)))

(table.insert state.clouds (make-cloud (. clouds 3) top-level 2 0))
(table.insert state.clouds (make-cloud (. clouds 3) (+ top-level 1) 11 0))
(table.insert state.clouds (make-cloud (. clouds 3) top-level 25 0))
(table.insert state.clouds (make-cloud (. clouds 3) (+ top-level 2) 33 0))
(table.insert state.clouds (make-cloud (. clouds 3) top-level 39 0))

(local god-lines
       ;; 1234567890-2-4-6--20-2-4-6--30-2--5\n1234567890-2-4-6--20-2-4-6--30-2--5\n1234567890-2-4-6--20-2-4-6--30-2--5\n
       [["WHAT'CHA DOIN?"
         "WORKING TOGETHER TO HELP EACH OTHER\nHAVE FOOD AND SAFE PLACES TO SLEEP?"
         "OH DEAR =( HAVE FUN 'WORKING       \nTOGETHER' WHEN YOU CAN'T TALK TO   \nEACH OTHER!"]
        ["WHY ARE YOU BUILDING A TOWER?"
         "TRYING TO ESCAPE MY PLAGUES,       \nFLOODS, FAMINE AND WAR?"
         "HAH! AS IF! TAKE THIS!"]
        ["YOU WERE MEANT TO SUFFER, YOU KNOW.\n\nMY CAR RUNS ON SUFFERING."
         "IT'S WHY I CREATED YOU. IT'S WHY   \nI SENT THE GIFTS: WAR. DISASTER.   \nFAMINE. PESTILENCE. EMPIRE. MONEY."
         "YOU'RE GOING AGAINST MY DIVINE     \nWILL, AND I *WILL* STOP YOU!"]
        ["*SIGH* YOU FOUND THE ONCE PLACE I  \nCAN'T SEND DESTRUCTION.            \nCOOL COOL COOL"
         "YOU THINK YOU'RE SO CLEVER?        \nYEAH?"
         "YOU'LL NEVER MAKE IT TO HEAVEN!"]
        ["YOUR PROJECT IS DOOMED TO FAILURE. \nHUMANS ARE DESTINED FOR CONFLICT,  \nNOT PEACEFUL COEXISTENCE."
         "YOU ARE FIGHTING THE NATURAL ORDER.\nI KNOW, I CREATED IT."
         "DON'T MAKE ME COME DOWN THERE AND  \nSMITE YOU MYSELF."]])

(fn say [lines sprite after ?who]
  (if state.said
      (table.insert state.to-say [lines sprite after ?who])
      (do
        (set state.said lines)
        (set (state.said.timer state.said.sprite) (values 180 sprite))
        (set (state.said.after state.said.who) (values after ?who)))))

(fn c-str [x y] (string.format "%dx%d" (// x 8) (// y 8)))
(fn claimed? [x y] (. state.claims (c-str x y)))

(fn claim [x y to] (tset state.claims (c-str x y) to))

(fn claim-on-level? [y tile]
  (accumulate [f false coord t (pairs state.claims) &until f]
    (and (coord:find (.. "x" (// y 8))) (= tile t))))

;; pixel coords, not tile coords
(fn mpget [x y] (mget (// x 8) (// y 8)))
(fn mpset [x y t] (mset (// x 8) (// y 8) t))

(for [x (+ state.build-min) (- state.build-max 16) 8]
  (for [y ground-level (* 124 8) -8]
    (claim x y :wall))) ; pre-existing ruins

;; tt = returns tile type, not number
(fn mpgett [x y] (. tiles (mpget x y)))

(fn height-of [{: y}] (+ 1 (// (- ground-level y) 8)))

(fn find-on-level [x y type]
  (let [matches (fcollect [sx state.build-min state.build-max 8]
                  (if (= type (mpgett sx y)) sx))]
    (case (doto matches (table.sort (sorter #(math.abs (- $ x)))))
      [mx] {:x mx : y})))

(fn find-target [self y check]
  (if (< 8 y)
      (or (faccumulate [spot nil x state.build-min state.build-max 8
                        &until spot]
            (if (and (= 0 (mpget x y)) (not (check x y)))
                {: x : y}))
          (find-target self (- y 8) check))))

(fn injury? [{: fatigue : x} tile]
  (let [r (+ 512 (math.random state.thresholds.rest))
        injury-count (accumulate [n 0 _ w (pairs state.workers)]
                       (if (< 0 w.injury) (+ n 1) n))]
    (when (and (or (= tile :wall) (= tile :floor)) (not= x stair-x)
               (. state.languages 3) (< r fatigue)
               (< injury-count (- (length state.languages) 2)))
      r)))

(fn build-tile [self tile]
  (while (< self.built build-time)
    (coroutine.yield)
    (inc self :built))
  (set (self.built self.carrying) 0)
  (update! self :fatigue + 32)
  (inc tempo :perc)
  (inc tempo :entropy)
  (set state.height (math.max state.height (height-of self)))
  (mpset self.x self.y (tile-num tile))
  (case (injury? self tile)
    injury (do (claim self.x self.y (if (= :wall tile) nil :wall))
               (dbg :injured self self.x self.y)
               (say ["ouch! that hurt\nI need medical attention"]
                    :worker nil self)
               (set self.injury injury)
               (self:check-needs))))

(fn build-stairs [self move]
  (when (or (= :wall (mpgett stair-x self.y))
            (= :floor (mpgett stair-x self.y)))
    (claim stair-x self.y :stairs)
    (move self {:x stair-x :y self.y} 1)
    (build-tile self :stairs)))

(fn move-by [self dx dy]
  (when (< dy 0)
    (update! self :climb-stamina - 1)
    (when (and (< self.climb-stamina 0) (< 0.9 (math.random)))
      (update! self :fatigue - 1)
      (update! self :hunger - 1)))
  (set (self.x self.y self.direction)
       (values (+ self.x dx) (+ self.y dy)
               (if (> 0 dy) :up
                   (< 0 dx) :right
                   :left))))

(fn wall-slice [y]
  (let [base (+ 7 (* 4 (// (// y 8) 4) 8))]
    [(+ base 16) (+ base 8) base]))

(fn same-floor? [{:y ay} {:y by}]
  (let [[max _ min] (wall-slice ay)]
    (<= min by max)))

(fn move [self target keep-task?]
  (assert target "move without target")
  (when (not keep-task?) (set self.task nil))
  (coroutine.yield)
  (when (and self.carrying (< 0.5 (math.random)))
    (coroutine.yield)) ; move slower when carrying
  (if (= self.y target.y)
      (move-by self (if (< self.x target.x) 1 -1) 0)
      (and (<= state.build-min self.x state.build-max)
           (or (= :stairs (mpgett self.x self.y))
               (same-floor? self target)))
      (move-by self 0 (if (< self.y target.y) 1 -1))
      (case (find-on-level self.x self.y :stairs)
        stairs (move self stairs keep-task?)
        ;; unclear why this happens; hack around for now!
        _ (when (= self.task :deicide)
            (move-by self 0 (if (< self.y target.y) 1 -1)))))
  (when (not (at? self target))
    (move self target keep-task?)))

(fn floor-level? [y] (= 3 (% (// y 8) 4)))

(fn claim-wall-or-floor [{: x : y}]
  (if (floor-level? y)
      (claim x y :wall)
      (each [_ ny (ipairs (wall-slice y))]
        (claim x ny :wall))))

(fn maybe-stairs [self]
  (when (and (not (find-on-level self.x self.y :stairs))
             (not (claim-on-level? self.y :stairs)))
    (build-stairs self move)))

(fn build-wall [{: x : y &as self}]
  (dbg :building-start x y)
  (each [_ ny (ipairs (wall-slice y))]
    (dbg :building x ny)
    (move self {: x :y ny})
    (build-tile self :wall)
    (inc tempo :perc)
    (maybe-stairs self)))

(fn build [self]
  (assert (= self.carrying :stone) "building without stone")
  (if (floor-level? self.y)
      (do (build-tile self :floor)
          (maybe-stairs self))
      (build-wall self)))

(fn queue-pos [{: queued-at : y}]
  (let [before (accumulate [b 0 _ w (ipairs state.workers)]
                 (if (and (= w.task :queue) (< w.queued-at queued-at))
                     (+ b 1)
                     b))]
    {:x (+ quarry.queue.x (* before 10)) : y}))

(fn quarry-queue [self]
  (set self.can-cancel true)
  (if (not state.quarry-a)
      (do (set state.quarry-a self)
          (set self.talking  (. self.languages 1))
          (move self quarry.a)
          (set self.direction :right)
          :quarry-a)
      (and (not state.quarry-b)
           (language-overlap? state.quarry-a self))
      (do (set state.quarry-b self)
          (let [shared-lang (language-overlap? state.quarry-a self)]
            (set self.talking shared-lang)
            (set state.quarry-a.talking shared-lang))
          (move self quarry.b)
          :quarry-b)
      (do (when (not state.quarry-b)
            (inc self :frustration))
          (coroutine.yield) ; wait
          (when self.requeue
            (move self (queue-pos self) true)
            (set self.direction :left)
            (set self.requeue false))
          (quarry-queue self))))

(fn queued-workers []
  (icollect [_ w (ipairs state.workers)]
    (if (= :queue w.task) w)))

(fn requeue []
  (each [_ w (ipairs state.workers)] (when (= :queue w.task) (set w.requeue true))))

(fn go-quarry [self]
  (set self.queued-at (time))
  (set (self.task self.queue-boredom self.canceled) (values :queue 0))
  (move self (queue-pos self) true)
  (let [q (quarry-queue self)]
    (requeue)
    (set self.task :wait)
    (while (and (not self.canceled) (not (and state.quarry-a state.quarry-b)))
      (inc self :queue-boredom)
      (when (and (< state.thresholds.boredom self.queue-boredom)
                 (next (queued-workers)))
        (set self.canceled true))
      (coroutine.yield))
    (inc self :dug)
    (while (and (< self.dug quarry-time) (not self.canceled))
      (set self.task :quarry)
      ;; both slots have to be claimed and begun to continue digging
      ;; otherwise one digger will start before the other
      (when (= :quarry state.quarry-a.task state.quarry-b.task)
        (inc self :dug)
        (inc self :fatigue))
      (coroutine.yield))
    (tset state q nil))
  (inc tempo :perc)
  (set (self.dug self.talking self.can-cancel) 0)
  (requeue)
  (if (not self.canceled)
      (case (find-target self self.y claimed?)
        target (do (set self.carrying :stone)
                   (set self.climb-stamina 200)
                   (claim-wall-or-floor target)
                   (move self target)
                   (build self)))
      (set self.canceled false)))

(fn help-needed? [self]
  (accumulate [h nil i help (ipairs state.need-help) &until h]
    (if (language-overlap? self help.from) i)))

(fn give-help [self i]
  (inc tempo :entropy)
  (set self.task :build)
  (let [{: from :order [x y w h blueprint]} (table.remove state.need-help i)
        shared-lang (language-overlap? self from)]
    (move self {: x :y (+ y h)} true)
    (set (from.need-help from.can-cancel) false)
    (set (self.talking from.talking) (values shared-lang shared-lang))
    (for [ty (// h 8) 1 -1]
      (for [tx 2 (// w 8) 2] ; same as follow-orders but every other tile
        (move self {:x (+ x -8 (* tx 8))
                    :y (+ y -8 (* ty 8))} true)
        (build-tile self (. blueprint ty tx))))
    (set (self.talking self.task) nil)))

(fn ask-for-help [self order]
  (table.insert state.need-help {: order :from self})
  (set (self.need-help self.talking) (values true (. self.languages 1)))
  (set (self.can-cancel self.task) (values true :build)))

(fn make-shop [x y w h shop-type]
  {: shop-type : x : y : w : h :staff nil
   :supply (if (= shop-type :food) supply-max)
   :target {:x (if (= :beds shop-type)
                   (+ x 4)
                   (+ x 16))
            :y (+ y 30)}})

(fn got-help [self [x y w h blueprint shop-type]]
  (for [ty (// h 8) 1 -1]
    (for [tx 1 (// w 8) 2]
      (move self {:x (+ x -8 (* tx 8))
                  :y (+ y -8 (* ty 8))} true)
      (build-tile self (. blueprint ty tx))))
  (when (and (not= shop-type :beds) (not (next state.shops)))
    (say ["whew, finished building.\nbut it still needs to be staffed."]
         :worker nil self))
  (table.insert state.shops (make-shop x y w h shop-type))
  (set (self.talking self.task self.order) nil))

(fn follow-orders [self [x y _w h &as order]]
  (set self.order order)
  (ask-for-help self order)
  (move self {: x :y (+ y h)} true)
  (while (and self.need-help (not self.canceled))
    (inc self :frustration)
    (when (< state.thresholds.leave-build self.frustration)
      ;; break out of the loop; give a chance to study, then leave if it fails
      (table.insert state.orders self.order)
      (set (self.canceled self.order) true)
      (each [i h (ipairs self.need-help)]
        (when (= h.from self) (table.remove self.need-help i))))
    (coroutine.yield))
  (if self.canceled
      (set (self.need-help self.canceled self.can-cancel self.talking) nil)
      (got-help self order)))

(fn find-shop [self shop-type ?pred]
  (let [shops (icollect [_ shop (ipairs state.shops)]
                (if (and (= shop-type shop.shop-type)
                         (not shop.occupied)
                         (or (= shop-type :beds) shop.staff)
                         (or (not ?pred) (?pred shop self)))
                    shop))]
    (table.sort shops (sorter (partial distance self)))
    (. shops 1)))

(local shop-tasks {:study :study :hunger :eat :fatigue :rest :medic :medic})

(fn use-shop [self shop field amount]
  (dbg :go-to-shop self shop.x shop.y)
  (set (shop.occupied self.task) (values self (. shop-tasks field)))
  (move self shop.target true)
  (set (shop.occupant-present self.occupying-shop) (values true true))
  (while (< 0 (. self field))
    (update! self field - amount)
    (coroutine.yield))
  (tset self field 0)
  (set (shop.occupied shop.occupant-present self.occupying-shop) (values nil nil nil)))

(fn try-eat [self]
  (dbg self :hungry!)
  (case (find-shop self :food #(and (language-overlap? self $.staff)
                                    (< 0 $.supply)))
    shop (do (use-shop self shop :hunger 4)
             (update! shop :supply - 1))
    _ (when (. state.languages 2)
        (inc self :frustration)
        (dbg :cant-eat self))))

(fn try-rest [self]
  (dbg self :tired)
  (case (find-shop self :beds)
    shop (use-shop self shop :fatigue 2)
    _ (when (. state.languages 2)
        (inc self :frustration)
        (dbg :cant-rest self))))

(fn find-need-staff [self]
  (accumulate [s nil i shop (ipairs state.need-staff) &until s]
    (if (find self.languages shop.language) (table.remove state.need-staff i))))

(fn staff [self shop]
  (dbg :staffing self shop.x shop.y)
  (inc tempo :entropy)
  (set (self.task self.hunger self.fatigue) (values :staff 0 0))
  (set shop.staff-coming true)
  (move self {:x (+ shop.x 8) :y (+ shop.y 16)} true)
  ;; if another worker has beaten us here, don't staff it; move on
  (when (not shop.staff)
    (set (shop.staff self.staffing) (values self shop))
    (set (shop.staff-coming shop.staff-requested) (values nil nil))
    (while self.staffing
      (coroutine.yield)
      (when (and shop.supply (<= shop.supply 0))
        (set self.staffing false)
        (say ["looks like we're out of food\ntime to restock"]
             :worker nil self)
        (move self farm)
        (move self shop)
        (set self.staffing shop)
        (set shop.supply supply-max))))
  (set self.task nil))

(fn unknown-languages [self]
  (icollect [_ lang (ipairs state.languages)]
    (if (not (find self.languages lang)) lang)))

(fn try-study [self]
  (let [to-learn (unknown-languages self)]
    (case (find-shop self :study
                     #(find to-learn (. $.staff.languages 1)))
      shop (let [[l1 l2] shop.staff.languages
                 language (if (find self.languages l1) l2 l1)]
             (set self.learn 200)
             (use-shop self shop :learn 1)
             (table.insert self.languages language)
             (set self.frustration 0)
             (dbg :learned self language))
      _ (dbg :cant-study self self.frustration (table.unpack to-learn)))))

(fn leave [self lines]
  (when (not= :deicide self.task)
    (say lines :worker #(set self.leaving true) self)
    (while (not self.leaving) (coroutine.yield))
    (set (self.task self.can-cancel) :leave)
    (move self exit true)
    (let [(_ i) (coroutine.yield)]
      (dbg :leaving self i self.task)
      (inc state :left)
      (table.remove state.workers i))))

(fn try-medic [self]
  (case (find-shop self :medic #(language-overlap? self $.staff))
    shop (do (use-shop self shop :injury 1)
             (set (self.frustration self.hunger) (values 0 0))
             (dbg :heal self))
    _ (do (coroutine.yield)
          (inc self :frustration)
          (if (< self.frustration state.thresholds.leave-medic)
              (try-medic self)
              (leave self ["I'll try to seek medical\nhelp elsewhere"]
                     :worker nil self)))))

(fn deicide [self]
  (claim self.x self.y nil) ; might help avoid gaps at endgame
  (move self {:x stair-x :y top-level} true)
  (when (not state.last-line)
    (say ["WAIT.. HOLD ON... STOP!" "PUT ME DOWN---"] :god
         #(set (state.god.dy state.god.ddy) (values -2 0.1)))
    (set state.last-line true))
  (move self {:x (+ state.god.x (* 4 self.d-team) -8)
              :y top-level} true)
  (inc state :deiciders)
  (while (< state.deiciders state.to-deicide)
    (coroutine.yield))
  (while (< 0 state.god.y) (coroutine.yield))
  (self:start))

(fn check-needs [self]
  (when (< state.thresholds.heal self.injury)
    (try-medic self))
  (when (< state.thresholds.eat self.hunger)
    (try-eat self))
  (when (< state.thresholds.rest self.fatigue)
    (try-rest self))
  (when (< state.thresholds.study self.frustration)
    (try-study self)))

(fn check-tasks [self]
  (case (find-need-staff self)
    shop (staff self shop))
  (case (help-needed? self)
    i (give-help self i))
  (case (table.remove state.orders 1)
    order (follow-orders self order)))

;; after building, head down towards the quarry, but check along the way
(fn move-down [self]
  (while (< self.y (- ground-level 1))
    (move self {:x stair-x :y (math.min (- ground-level 1) (+ self.y 16))})
    (check-needs self)
    (check-tasks self)))

(fn start [self]
  (coroutine.yield)
  (go-quarry self)
  (check-needs self)
  (check-tasks self)
  (check-needs self)
  (when (< state.thresholds.leave-hunger self.hunger)
    (leave self ["whew, I'm starving.\nI need to leave to find food."]))
  (when (< state.thresholds.leave-frustration self.frustration)
    (leave self ["no one understands me.\nI'm tired of this."]))
  (when (< state.thresholds.leave-fatigue self.fatigue)
    (leave self ["I'm exhausted.\nI can't do this any more."]))
  (move-down self)
  (start self))

(fn make-worker [x y]
  (inc tempo :entropy)
  (let [self {:languages [(randomchoice state.languages)]
              :sprite 257 : x : y :w 8 :h 16 :direction :left
              :hunger 0 :dug 0 :built 0 :frustration 0 :learn 0
              :fatigue 0 :queue-boredom 0 :climb-stamina 0
              :carrying nil :staffing nil :injury 0
              :coro (coroutine.wrap start) : start : check-needs}
        id (: (tostring self) :sub -4)]
    (setmetatable self {:__tostring #id})
    (fn self.tick [_ i]
      (when (not self.staffing)
        (when (< state.hunger-chance (math.random))
          (inc self :hunger)))
      (self:coro i))
    self))

(fn speakers-of [lang]
  (accumulate [n 0 _ w (ipairs state.workers)]
    (if (find w.languages lang) (+ n 1) n)))

(fn babel-incident [_said recur?]
  (set state.cy (- ground-level)) ; can't see the shake if scrolled to top
  (update! state :hunger-chance - 0.02)
  (when (not recur?)
    (table.insert state.languages (+ (last state.languages) 1)))
  (assert (<= (length state.languages) 6) "ran out of languages")
  (each [_ worker (pairs state.workers)]
    ;; change to (length worker.languages) to preserve bilingual
    (let [lang-count 1]
      (while (next worker.languages) (table.remove worker.languages))
      (for [_ 1 lang-count]
        (table.insert worker.languages (randomchoice state.languages)))))
  (set state.shake (* state.height 4))
  (set tempo.perc (+ tempo.perc (length state.workers)))
  (set tempo.entropy (+ tempo.entropy state.height))
  ;; this is a hacky workaround for shops getting stuck in WAIT,
  ;; I haven't found the root cause but this should make it less
  ;; frustrating. it does open up the possibility of double-staffing.
  (each [_ shop (pairs state.shops)]
    (set shop.staff-requested nil))
  (when (not (accumulate [ok true _ l (ipairs state.languages) &until (not ok)]
               (< 1 (speakers-of l))))
    (babel-incident _said true)))

;; take the highest 8 workers, cancel whatever they're doing an
(fn initiate-deicide []
  (say ["HOW DID YOU GET UP HERE???         \nDID I HAVE A BUG IN MY CODE?"
        "UH OH, HOW DO I LOG OUT?           \nMOM! COME DOWN TO THE BASEMENT!    \nQUICK!"] :god)
  (set state.speed 1)
  (let [workers (doto (copy state.workers)
                  (table.sort (sorter #$.y)))]
    (set state.to-deicide (math.min 6 (length workers)))
    (for [i 1 (math.min 8 (length workers))]
      (let [w (. workers i)]
        (set (w.task w.d-team w.carrying w.talking) (values :deicide i))
        (set w.coro (coroutine.wrap deicide))))))

(local worker-join-dialog
 ;; 1234567890-2-4-6--20-2-4-6--30-2--5\n1234567890-2-4-6--20-2-4-6--30-2--5\n1234567890-2-4-6--20-2-4-6--30-2--5\n
  ["our home is flooded, can we help?"
   "you're growing food here!          \nlet us help"
   "our village burned, can we help?"
   "we've escaped the war,             \nwe're here to help"])

(fn follow [{: y}]
  (when (< state.cy (- 16 y))
    (set state.cy (- 16 y)))
  (when (< (- 120 y) state.cy)
    (set state.cy (- 120 y))))

(fn arrive [say?]
  (tset state.arrivals state.height true)
  (let [worker (make-worker (+ 240 (math.random 150)) (- ground-level 1))]
    (dbg :arrive worker)
    (when say?
      (say [(randomchoice worker-join-dialog)] :worker nil worker))
    (table.insert state.workers worker)
    worker))

(local win-text
       "
you have successfully built a tower
to the heavens, dethroned a tyrant,
and now you and your community can
work together in peace

  well done comrade



thank you for playing

  you finished with %s workers leaving






if you are curious how the game
was made, press escape and poke
around a bit")

(fn congratulations []
  (cls 3)
  (update! state :win-scroll + 0.25)
  (print2 (win-text:format state.left)
          16 (- 161 state.win-scroll) 6 8))

(fn update []
  (let [height state.height]
    (for [i (length state.workers) 1 -1]
      (: (. state.workers i) :tick i))
    (each [_ c (ipairs state.clouds)]
      (set c.x (% (+ c.x c.dx) 248)))
    (when (< height state.height)
      ;; (when (and (= height 18) (= state.build-max 96))
      ;;   (say ["the tower is getting too tall!"
      ;;         "we need to widen it"] :worker
      ;;         #(set state.build-max 120)))
      (when (and (= 0 (% (- state.height 4) 4))
                 (< state.height 100)
                 (not (. state.arrivals state.height)))
        (let [arrival (arrive true)]
          (when (. state.languages 4) ; family of 2 or 3
            (doto (arrive) (tset :languages (copy arrival.languages)))
            (when (< 0.5 (math.random))
              (arrive)))))
      (when (= (height-of {:y top-level}) state.height)
        (initiate-deicide))
      (when (= 0 (% state.height 16))
        (case (table.remove god-lines 1)
          lines (say lines :god babel-incident)))))
  (set (state.god.y state.god.dy) (values (+ state.god.y state.god.dy)
                                          (+ state.god.dy state.god.ddy)))
  (when (< 1200 state.god.y)
    (tset _G :TIC congratulations))
  (when state.said
    (if (and (= state.said.sprite :god) (<= state.cy (- cloud-level)))
        (update! state :cy + (if (< (+ state.cy cloud-level) -512) 16
                                 (< (+ state.cy cloud-level) -64) 4
                                 1))
        (update! state.said :timer - 1))
    (when (or (< state.said.timer 0) (btnp 4))
      (table.remove state.said 1)
      (set state.said.timer 180))
    (when (not (. state.said 1))
      (when state.said.after (state.said.after state.said))
      (set state.said nil)
      (when (. state.to-say 1)
        (say (table.unpack (table.remove state.to-say 1))))))
  (when (and (not state.said) state.focused)
    (follow state.focused)))

(fn build-rect [x y selected]
  (inc tempo :entropy)
  (let [bp (. state.blueprints selected)
        mx (align (- x state.cx)) my (align (- y state.cy))
        clear? (accumulate [f true [dx dy] (iter-rect bp) &until (not f)]
                 (= :wall (mpgett (+ mx (* 8 (- dx 1)))
                                  (+ my (* 8 (- dy 1))))))]
    (values clear? mx my (* 8 (length (. bp 1))) (* 8 (length bp)))))

(local dialog-sprites {:food 291 :beds 292 :medic 307 :study 308})

(fn draw-dialog [{: choices :xywh [x y w h]}]
  (rect x y w h 6)
  (rectb x y w h 8)
  (each [i choice (ipairs choices)]
    (if (= :string (type choice))
        (let [sprite (. dialog-sprites choice)]
          (if sprite
            (do (spr sprite (+ x 2) (+ y (* i 8) -6) 7)
                (print choice (+ x 10) (+ y (* i 8) -6) 0))
            (print choice (+ x 2) (+ y (* i 8) -6) 0)))
        (rect (+ x 2) (+ y (* i 8) -6) (- w 4) 8 choice))))

(local hud-y {:workers 2 :hungry 10 :tired 18 :frustrated 26 :injured 34
              :sep 43
              :food 46 :study 54 :medic 62 :beds 70
              :focus-start 76 :focus-end 126
              :focus-worker 78 :focus-hunger 86 :focus-tired 94
              :focus-frustrated 102 :focus-injure 110
              :focus-task 118})

(fn title-for [title y]
  (if (< y 43)
      (.. "total " title)
      title))

(fn draw-ui []
  (if state.dialog
      (draw-dialog state.dialog)
      state.selected
      (let [(mx my) (mouse)
            (clear? x y w h) (build-rect mx my state.selected)
            x (+ x state.cx) y (+ y state.cy)]
        (rectb x y w h (if clear? 7 0))))


  (spr 259 220 hud-y.workers 7)    ;; worker count
  (spr 260 222 hud-y.hungry 7)     ;; hungry count
  (spr 275 222 hud-y.tired 7)      ;; tired count
  (spr 276 222 hud-y.frustrated 7) ;; frustrated count
  (spr 261 222 hud-y.injured 7)    ;; injured count
  (line 223 hud-y.sep 227 hud-y.sep 5)
  (spr 291 222 hud-y.food 7)       ;; food shops
  (spr 308 222 hud-y.study 7)      ;; study shops
  (spr 307 222 hud-y.medic 7)      ;; medic shops
  (spr 292 222 hud-y.beds 7)       ;; beds
  (let [(mx my) (mouse)] ; status tooltips
    (when (< 220 mx)
      (each [title y (pairs hud-y)]
        (when (and (<= y my (+ y 8) 80) (not= title :sep))
          (let [txt (title-for title y)
                w (+ 10 (* (length txt) 4))
                h 10 x (- mx w 5)]
            (rect x my w h 6)
            (rectb x my w h 8)
            (print txt (+ x 2) (+ my 2) 0 false 1 true))))))
  (each [i lang (ipairs state.languages)]
    (let [t state.thresholds
          lw (accumulate [m {:w 0 :h 0 :t 0 :f 0 :i 0 :sf 0 :ss 0 :sm 0} _ w (ipairs state.workers)]
               (if (find w.languages lang)
                   (let [staffing w.staffing]
                     {:w (+ m.w 1)
                      :h (+ m.h (if (> w.hunger t.eat) 1 0))
                      :t (+ m.t (if (> w.fatigue t.rest) 1 0))
                      :f (+ m.f (if (> w.frustration t.study) 1 0))
                      :i (+ m.i (if (> w.injury t.heal) 1 0))
                      :sf (+ m.sf (if (and staffing (= :food staffing.shop-type)) 1 0))
                      :ss (+ m.ss (if (and staffing (= :study staffing.shop-type)) 1 0))
                      :sm (+ m.sm (if (and staffing (= :medic staffing.shop-type)) 1 0))})
                 m))]
      (when (< 0 lw.w) (let [y (+ -1 hud-y.workers i)] (line (- 221 lw.w) y 220 y (lang-color lang))))
      (when (< 0 lw.h) (let [y (+ -1 hud-y.hungry i)] (line (- 221 lw.h) y 220 y (lang-color lang))))
      (when (< 0 lw.t) (let [y (+ -1 hud-y.tired i)] (line (- 221 lw.t) y 220 y (lang-color lang))))
      (when (< 0 lw.f) (let [y (+ -1 hud-y.frustrated i)] (line (- 221 lw.f) y 220 y (lang-color lang))))
      (when (< 0 lw.i) (let [y (+ -1 hud-y.injured i)] (line (- 221 lw.i) y 220 y (lang-color lang))))
      (when (< 0 lw.sf) (let [y (+ hud-y.food i)] (line (- 221 lw.sf) y 220 y (lang-color lang))))
      (when (< 0 lw.ss) (let [y (+ hud-y.study i)] (line (- 221 lw.ss) y 220 y (lang-color lang))))))
  (let [sa (accumulate [m {:b 0 :f 0 :s 0 :m 0} _ s (ipairs state.shops)]
             (if (= :beds s.shop-type) (do (inc m :b) m)
                 (and (not s.staff) (= :food s.shop-type)) (do (inc m :f) m)
                 (and (not s.staff) (= :study s.shop-type)) (do (inc m :s) m)
                 (and (not s.staff) (= :medic s.shop-type)) (do (inc m :m) m)
                 m))]
    (when (< 0 sa.f) (line (- 221 sa.f) hud-y.food 220 hud-y.food 2))
    (when (< 0 sa.s) (line (- 221 sa.s) hud-y.study 220 hud-y.study 2))
    (when (< 0 sa.m) (line (- 221 sa.m) hud-y.medic 220 hud-y.medic 2))
    (when (< 0 sa.b) (line (- 221 sa.b) hud-y.beds 220 hud-y.beds 2)))
  (spr 277 231 52 7)
  (for [i 1 (// state.height 2) 1]
    (spr (+ 320 (% i 8)) 232 (- 52 i) 7))
  (each [_ s (ipairs state.shops)]
    (let [x (// (- s.x 56) 8)
          y (// (- 992 s.y) 16)
          w (// s.w 16)]
      (rect (+ x 232) (- 51 y) w 1 0)))
  (let [y (// (+ state.cy bottom-level) 16)]
    (spr 306 238 (- 49 y) 7))

  (print2 "[BUILD]" 205 129 0 5)
  (print2 "[EASY   ]" 158 129 0 5)
  (when state.easy (print2 "X" 188 129 0 5))
  (when state.pause
    (print "P A U S E" 38 58 6 false 3)
    (print "P A U S E" 42 62 1 false 3)
    (print "P A U S E" 40 60 0 false 3)))

(fn draw-said-worker [the-line] ; 3-line max; ~35 chars
  (do (rect 0 108 240 28 6)
      (rectb 1 109 238 26 5)
      (when state.said.who
        (let [[l1 l2] state.said.who.languages]
          (spr (+ 297 l1) 3 110 7 2 1)
          (when l2 (spr (+ 297 l2) 5 112 7 2 1))))
      (print the-line 36 113 0)
      (spr 256 15 115 7 2)))

(fn draw-said-god [line] ; 2-line max
  (rect 20 (+ state.cy cloud-level 46) 202 25 6)
  (rect 21 (+ state.cy cloud-level 47) 200 23 5)
  (print line 24 (+ state.cy cloud-level 50) 9))

(fn draw-said []
  (case state.said
    [line] (if (= state.said.sprite :god)
               (draw-said-god line)
               (draw-said-worker line))))

(fn draw-staffing [shop]
  (let [x (+ shop.x 1 state.cx) y (+ shop.y 16 6 state.cy)]
    (if shop.staff-coming (let [y (- y (% (/ tempo.tic 4) 16))]
                            (rect x y 6 2 (lang-color shop.language)))
        shop.staff-requested (rect x y 6 2 (lang-color shop.language))))
  (when (not shop.staff)
    (when (not (= :beds shop.shop-type))
      (let [x (+ shop.x state.cx) y (+ shop.y state.cy 8)]
        (spr 146 (+ 24 x) y 15 1 0 0 1 2)
        (spr 144 (+ 8 x) y 15 1 0 0 2 2)))))

(fn draw-shop-decoration [shop]
  (let [x (+ shop.x state.cx) y (+ shop.y state.cy)]
    (case shop ; draw-flag
      {:staff {:languages [l1 ?l2]}}
      (let [(f1 f2) (lang-flag l1)
            (_ f3) (if ?l2 (lang-flag ?l2))]
        (when (and shop.staff shop.staff.staffing)
          (spr 290 (+ x 23) (+ y 16) 0))
        (spr f1 x y 0)
        (spr f2 x (+ y 8) 0)
        (spr (or f3 f2) x (+ y 16) 0)))
    (draw-staffing shop)
    (when shop.supply
      (rect (+ x 8) (- y -23 shop.supply) 3 shop.supply 9))
    (when shop.occupant-present
      (let [x (+ shop.x state.cx)
            y (+ shop.y state.cy 8)]
        (case shop.shop-type
          :beds (spr 229 x y 15 1 0 0 2 2)
          :food (spr 224 (+ 8 x) y 15 1 0 0 2 2)
          :study (spr 227 (+ 8 x) y 15 1 0 0 2 2)
          :medic (spr 231 (+ 8 x) y 15 1 0 0 2 2))))))

(fn bar-percent [bs be max amt]
  (let [amt (math.min max amt)
        pct (/ amt max)
        pt (math.max (* (- bs be) pct) 0)]
    (- bs pt)))

(fn draw-bar [val sprite hy thr max]
  (let [be 203 bs 227
        y (+ 3 hy)
        c (if (<= max val) 12 0)] ; over max goes red
    (spr sprite 230 hy 7)
    (line be (+ 1 y) bs (+ 1 y) 5)
    (pix (bar-percent bs be max thr) (+ 1 y) 2)
    (line (- be 1) y (- be 1) (+ 1 y) 2)
    (line (bar-percent bs be max val) y bs y c)))

(fn draw-focus [self lx uy]
  (let [rx (+ lx 11) dy (+ uy 19) c 9]
    (line lx uy (+ lx 3) uy 9) (line rx uy (- rx 3) uy c)
    (line lx dy (+ lx 3) dy 9) (line rx dy (- rx 3) dy c)
    (line lx uy lx (+ uy 3) 9) (line rx uy rx (+ uy 3) c)
    (line lx dy lx (- dy 3) 9) (line rx dy rx (- dy 3) c))
  (rect 199 (- hud-y.focus-start 1) 42 (- hud-y.focus-end hud-y.focus-start -2) 4)
  (rect 200 hud-y.focus-start 40 (- hud-y.focus-end hud-y.focus-start) 6)
  (spr 259 228 hud-y.focus-worker 7)
  (each [i l (ipairs self.languages)]
    (rect (- 230 (* i 6)) hud-y.focus-worker 4 4 (lang-color l)))
  (draw-bar self.hunger 260 hud-y.focus-hunger
            state.thresholds.eat state.thresholds.leave-hunger)
  (draw-bar self.fatigue 275 hud-y.focus-tired
            state.thresholds.rest state.thresholds.leave-fatigue)
  (draw-bar self.frustration 276 hud-y.focus-frustrated
            state.thresholds.study state.thresholds.leave-frustration)
  (draw-bar self.injury 261 hud-y.focus-injure
            state.thresholds.heal state.thresholds.leave-medic)
  (when self.task
    (print self.task 203 hud-y.focus-task 0 false 1 true)))

(fn draw-worker [{: x : y : carrying : direction : hunger &as self}
                 {: cx : cy}]
  (when (not self.occupying-shop)
  ;; y coord is the feet
    (let [px (+ x cx) py (+ y -15 cy)
          use-alt-sprite (-> tempo.tic (% 30) (/ 15) math.floor (= 0))]
        (when (= carrying :stone)
          (spr 272 (+ px (if (= direction :left) -2 2)) (+ py 4) 15))
        (when (= (?. state.said :who) self)
          (rectb px py 8 16 7))
        (when (= self state.focused)
          (draw-focus self (- px 2) (- py 2)))
        (when (< state.thresholds.eat hunger)
          (spr 257 px (- py 6) 7))
        (when (< state.thresholds.rest self.fatigue)
          (spr 258 px (- py 10) 7))
        (when (< state.thresholds.study self.frustration)
          (spr 273 px (- py 1) 7))
        (when (< state.thresholds.heal self.injury)
          (spr 274 px (+ py 6) 7))
        (when self.talking
          (let [sprite (+ self.talking 297)]
            (spr sprite px (- py 8) 7)))
        (for [i 1 3 1]
          (spr (+ 313 (or (. self.languages i)
                          (. self.languages (length self.languages))))
               px (- py -8 (case i 1 3 2 6 3 9))
               7))
        (spr (if use-alt-sprite 289 288) px py 7 1 (if (= direction :left) 0 1) 0 1 2))))


(fn draw []
  (cls 7)
  (when (< 0 state.shake)
    (poke 0x3FF9 (math.random -4 4))
    (poke 0x3FFA (math.random -4 4))
    (set state.shake (- state.shake 1))
    (when (= state.shake 0) (memset 0x3FF9 0 2)))
  (map 0 0 60 135 state.cx state.cy)
  (each [_ shop (ipairs state.shops)]
    (draw-shop-decoration shop))
  (each [_ c (ipairs state.clouds)]
    (spr c.sprite c.x (+ state.cy c.y) 7 1 0 0 c.w c.h))
  (let [{: sprite : x : y} state.god]
    (spr sprite x (+ y state.cy)
         7 1 0 0 4 4))
  (each [_ w (ipairs state.workers)]
    (when (not w.staffing)
      (draw-worker w state)))
  (draw-ui)
  (draw-said))

(fn get-button [mx my]
  (let [[dx dy dialog-w] state.dialog.xywh
        i (+ (// (- my dy) 8) 1)]
    (if (<= dx mx (+ dx dialog-w)) i)))

(fn get-click-target [click-x click-y items]
  (accumulate [i nil _ item (pairs items) &until i]
    (if (and (<= item.x click-x (+ item.x item.w))
             (<= item.y click-y (+ item.y item.h)))
        item)))

(fn destaff [shop]
  (let [worker shop.staff]
    (set (shop.staff shop.staff-requested) nil)
    (set worker.staffing nil)))

(fn staff-dialog [shop]
  (if (not= :beds shop.shop-type)
      {:choices (if shop.occupied ["(BUSY)"]
                    shop.staff ["LEAVE"]
                    shop.staff-requested ["(WAIT)"]
                    (icollect [_ l (ipairs state.languages)] (lang-color l)))
       :action (if shop.occupied #nil ; click does nothing
                   shop.staff
                   #(when (and (= 1 $) (not shop.occupied))
                      (destaff shop))
                   shop.staff-requested #nil ; I SAID WAIT
                   #(when $
                      (table.insert state.need-staff
                                    (doto shop
                                      (tset :language $)
                                      (tset :staff-requested true)))))
       :xywh [96 48 36 (math.max 28 (+ (* (length state.languages) 8) 4))]}))

(fn build-dialog []
  (let [choices [:food :beds :study :medic]]
    {: choices
     :action #(set state.selected (. choices $))
     :xywh [96 48 42 34]}))

(fn overlap? [[x1 y1 w1 h1] [x2 y2 w2 h2]]
  (let [xo (or (<= x2 x1 (+ x2 w2))
               (<= x1 x2 (+ x1 w1)))
        yo (or (<= y2 y1 (+ y2 h2))
               (<= y1 y2 (+ y1 h1)))]
    (and xo yo)))

(fn pending-build? [rect]
  (or (accumulate [y false _ order (ipairs state.orders) &until y]
        (overlap? rect order))
      (accumulate [y false _ worker (ipairs state.workers) &until y]
        (and worker.order (overlap? rect worker.order)))))

(fn toggle-easy []
  (each [k (pairs state.thresholds)]
    (when (k:find "leave")
      (if state.easy
          (update! state.thresholds k / 2)
          (update! state.thresholds k * 2))))
  (set state.easy (not state.easy)))

(fn click [mx my]
  (if (and (= :worker (?. state :said :sprite)) (<= 108 my))
      (set state.said.timer 0)
      (and (= :god (?. state :said :sprite))
           (<= 20 mx 222) (<= (+ state.cy cloud-level 46) my
                              (+ state.cy cloud-level 46 25)))
      (set state.said.timer 0)
      (and state.dialog (not state.pause))
      (let [choice-num (get-button mx my)]
        (when (. state.dialog.choices choice-num)
          (state.dialog.action choice-num))
        (set state.dialog nil))
      (and state.selected (not state.pause))
      (let [(clear? x y w h) (build-rect mx my state.selected)
            bp (. state.blueprints state.selected)]
        (when (and clear? (not (pending-build? [x y w h])))
          (table.insert state.orders [x y w h bp state.selected]))
        (set state.selected nil))
      (and (<= 205 mx) (<= 125 my) (not state.pause)) ; build button
      (set state.dialog (build-dialog))
      (and (<= 155 mx 205) (<= 125 my)) ; easy button
      (toggle-easy)
      ;; (and (<= 170 mx 190) (<= 79 my 89) (not state.pause)
      ;;      (?. state.focused :can-cancel)) ; cancel button
      ;; (set state.focused.canceled true)
      (let [click-x (- mx state.cx) click-y (- my state.cy)]
        (case (get-click-target click-x (+ click-y 15) state.workers)
          worker (set state.focused worker)
          (where _ (not state.pause))
          (case (get-click-target click-x click-y state.shops)
            shop (set state.dialog (staff-dialog shop))
            _ (set state.focused nil))))))

(fn input []
  (when (keyp 50) (update! state :pause not)) ; enter
  (when (keyp 48) (when (not state.pause)
                    (set state.dialog (build-dialog)))) ; space
  (when (keyp 02) (when (not state.pause)
                    (set state.dialog (build-dialog)))) ; b
  (when (keyp 28) (set state.selected :food)) ; 1
  (when (keyp 29) (set state.selected :beds)) ; 2
  (when (keyp 30) (set state.selected :study)) ; 3
  (when (keyp 31) (set state.selected :medic)) ; 4
  (when (keyp 56) (set state.cy (- cloud-level))) ; home
  (when (keyp 57) (set state.cy (- ground-level))) ; end
  (when (keyp 54) (update! state :cy + 132)) ; pageup
  (when (keyp 55) (update! state :cy - 132)) ; pagedown
  (when (keyp 51) (toggle-easy)) ; backspace
  (when (keyp 49) (set state.speed (case state.speed 1 4 _ 1))) ; tab
  (when (btnp 5) (set state.speed (case state.speed 1 16 _ 1))) ; x
  (when (btn 0) (update! state :cy - (if (key 64) -8 -1))) ; scroll
  (when (btn 1) (update! state :cy - (if (key 64) 8 1)))
  (let [[mx my mb _ _ _ scy &as this-mouse] [(mouse)]]
    (update! state :cy + (* 8 scy))
    (when (< my 5) (update! state :cy + 2))
    (when (< 132 my 251) (update! state :cy - 2))
    (when (and mb (not (. state.last-mouse 3)))
      (click mx my))
    (set state.last-mouse this-mouse))
  (set state.cx (-> state.cx (math.max -240) (math.min 0)))
  (set state.cy (-> state.cy (math.max (- bottom-level)) (math.min -168))))

(fn make-euclid-pattern [steps pulses offset]
  (var pile (- steps pulses))
  (local pat [])
  (when (< 0 steps)
    (for [s 0 (- steps 1) 1]
      (let [sidx (-> s (+ offset steps) (% steps))]
        (set pile (+ pile pulses))
        (when (>= pile steps)
          (set pile (- pile steps))
          (table.insert pat sidx)))))
  pat)

(fn make-sequence [len]
  (local seq [])
  (for [_ 1 len 1]
    (table.insert seq (math.random)))
  (let [self {: seq :pos 1 : len}]
    (fn self.length [_ n]
      (when (> n (length self.seq))
        (for [_ 1 (- n (length self.seq)) 1]
          (table.insert self.seq (math.random))))
      (set self.len n))
    (fn self.replace [_] (tset self.seq self.pos (math.random)))
    (fn self.shift [_]
      (let [this-val (. self.seq self.pos)]
        (set self.pos (-> self.pos (+ 1) (% self.len) (+ 1)))
        this-val))
    self))

(local music-scales 
  {:pmaj [0 2 4 7 9]
   :bmaj [0 2 5 7 9]
   :susp [0 2 5 7 10]
   :pmin [0 3 5 7 10]
   :dim [0 3 6 9]})

;; 1/oct -> 12/oct TET
(fn quantize-note [val scale]
  (let [oct (math.floor val)
        scl-idx (-> val (or 0) (% 1) (* (length scale)) math.ceil)]
    ;; adding 4 at the end, the tonic is E, because I'm difficult like that
    (-> scale (. scl-idx) (or 0) (+ 4) (+ (* oct 12)))))

(fn make-trigger-seq [steps pulses offset len]
  (let [step-len (/ len steps)
        pat (collect [_ v (pairs (make-euclid-pattern steps pulses offset))]
              (values (* v step-len) true))
        self {: pat : len}]
    (dbg "trigger" (table.concat (icollect [k _ (pairs pat)] k) ","))
    (fn self.step [t] (. self.pat (% t self.len)))
    self))

(fn make-rhythm [steps pulses accents offset len]
  (let [pulses (math.min pulses steps)
        steps (if (= 0 pulses) 0 steps)
        pulse-pts (make-euclid-pattern steps pulses offset)
        accent-pts (collect [_ v (pairs (make-euclid-pattern pulses accents 0))]
                     (values v 0))
        step-len (/ len steps)
        self {: len
              :pat (collect [idx v (pairs pulse-pts)]
                     (values (* v step-len)
                             {:vol (if (. accent-pts idx) 12 6)}))}]
    (fn self.step [t] (. self.pat (% t self.len)))
    self))

(fn make-rhythmbox [len]
  (let [self { :pos 1 :seq [] : len}]
    (fn self.current [] (. self.seq self.pos))
    (fn self.step [t] (let [r (. self.seq self.pos)] (r.step t)))
    (fn self.set [p rs]
      (let [new-rhythm (make-rhythm rs.steps rs.pulses rs.accents rs.offset rs.len)]
        (set new-rhythm.speed rs.speed)
        (tset self.seq p new-rhythm)
        new-rhythm))
    (fn self.replace [spd rs]
      (self.set self.pos spd rs))
    (fn self.shift [_]
      (let [this-rhythm (. self.seq self.pos)]
        (set self.pos (-> self.pos (+ 1) (% self.len) (+ 1)))
        this-rhythm))
    self))

(fn sfxLRrand [base]
  (case (math.random 1 3)
    1 base
    2 (+ base 16)
    3 (+ base 32)))

(fn play-voice [voice tic qscale]
  (when (and voice.pitchsched
             (= 0 (% tic voice.pitchchange.len))
             (< (math.random) 0.2)
             (< 0 tempo.entropy))
    (set tempo.entropy (- tempo.entropy 1))
    (voice.pitchsched))

  (when (and voice.banger
             (= 0 (% tic voice.rhythm.len)))
    (when voice.rhythm.shift
      (voice.rhythm.shift))
    (when (or voice.every-banger
              (and (< (math.random) 0.2)
                   (< 1 tempo.entropy)))
      (when (not voice.every-banger)
        (set tempo.entropy (- tempo.entropy 1)))
      (voice.banger)))

  (when (and voice.pitchchange (voice.pitchchange.step tic))
    (when (and voice.pitchseq
               (< (math.random) 0.2)
               (< 0 tempo.entropy))
      (set tempo.entropy (- tempo.entropy 1))
      (voice.pitchseq.replace))
    (set voice.pitch (voice.pitchseq.shift)))

  (if (not (= 0 state.shake))
      (when (< (math.random) 0.07)
        (sfx (let [base (sfxLRrand 8)] (math.random base (+ base (- (length state.languages) 1))))
             (+ (math.random 60 66) (* voice.channel 3))
             -1 voice.channel (math.random 6 12) (math.random -1 2)))

    (case (voice.rhythm.step tic)
      {: vol} (sfx (voice.sfx (length state.languages)) 
                   (voice.notepitch qscale) 
                   -1 voice.channel vol (or voice.rhythm.speed voice.speed)))))

(local voices
  {:perc
   (let [self
         {:channel 0 :speed 0 :every-banger true}]
     (fn self.sfx [_] (math.random 0 2))
     (fn self.notepitch [qscale]
       (math.random 0 (-> state.height
                          (/ 100)
                          (* 2)
                          math.floor
                          (+ 0.5)
                          (quantize-note qscale))))
     (fn self.banger []
       (if (= 0 tempo.perc)
           (set self.rhythm (make-rhythm 4 0 0 0 tempo.beat))
           (let [perc tempo.perc ;;(// tempo.perc 2)
                 steps (if (> perc 6) 6
                           (> (math.random) 0.5) 4 6)
                 pulses (math.min perc steps)
                 accents (math.random 1 (-> perc (- pulses) (math.min (// steps 2)) (math.max 1)))]
             (set tempo.perc (-> perc (- pulses (- accents 1)) (math.max 0) (// 2) math.floor))
             (set self.rhythm (make-rhythm steps pulses accents 0 tempo.beat)))))
     (self.banger)
     self)

   :bass
   (let [self 
         {:channel 1 :speed -4
          :pitch 0
          :pitchseq (make-sequence 4)
          :pitchchange (make-trigger-seq 4 4 0 (* 4 tempo.bar))
          :rhythm (make-rhythmbox 4)
          :offset 0 :len (* 4 tempo.bar)}]
     (fn self.notepitch [qscale]
       (-> self.pitch (+ 1) (quantize-note qscale)))
     (fn self.sfx [langs] (+ 15 (math.random 1 (math.min 6 langs))))
     (fn self.rhythm-params []
       (let [tpl (> (math.random) 0.5)
             [steps pulses accents speed]
             (if (< state.height 8) [4 4 1 -4]
                 (< state.height 20) (if tpl [6 (math.random 4 6) 2 -4]
                                             [8 (math.random 4 7) (math.random 1 2) -4])
                 (< state.height 50) (if tpl [12 (math.random 6 8) 3 -3]
                                             [16 (math.random 6 12) (math.random 3 5) -3])
                 (if tpl [24 (math.random 6 18) (math.random 4 8) -2]
                         [32 (math.random 8 24) (math.random 4 10) -1]))]
         {: steps : pulses : accents : speed :offset self.offset :len self.len}))
     (fn self.banger [] (self.rhythm.replace (self.rhythm-params)))
     (for [i 1 4 1]
       (self.rhythm.set i (self.rhythm-params)))
     self)

   :harm1
   (let [self {:channel 2 :speed -2
               :pitch 0
               :pitchseq (make-sequence 8)
               :rhythm (make-rhythmbox 4)}]
     (fn self.notepitch [qscale]
       (-> self.pitch
           (* (math.min 6 (length state.languages)))
           (+ 1.5)
           (quantize-note qscale)))
     (fn self.sfx [l] (+ (sfxLRrand 15) (math.random 1 (math.min 6 l))))
     (fn self.pitchsched []
       (let [newlen (math.random 4 (+ 6 (math.floor (/ state.height 10))))]
         (set self.pitchchange (make-trigger-seq 24 newlen 0 (* 2 tempo.bar)))))
     (self.pitchsched)
     (fn self.rhythm-params []
       (let [len (math.random 1 4)
             steps (if (> (math.random) 0.5) (* len 8) (* len 12))
             pulse-base (* 3 len)
             pulse-max (+ pulse-base (math.ceil (/ state.height 25)))
             pulses (math.random pulse-base pulse-max)
             accents (math.random 1 (- pulses 1))
             offset (math.random 0 len)]
         { : steps : pulses : accents : offset :len (* len tempo.bar)}))
     (fn self.banger []
         (self.rhythm.replace (self.rhythm-params)))
     (for [i 1 4 1]
       (self.rhythm.set i (self.rhythm-params)))
     self)

   :harm2
   (let [self {:channel 3 :speed -1 :pitch 0 :pitchseq (make-sequence 24)
               :pitchchange (make-trigger-seq 24 24 0 tempo.bar)
               :rhythm (make-rhythmbox 4)}]
     (fn self.notepitch [qscale]
       (-> self.pitch
           (* (math.random 0 (length state.languages)))
           (+ 1)
           (quantize-note qscale)))
     (fn self.sfx [l] (+ (sfxLRrand 15) (math.random 1 (math.min 6 l))))
     (fn self.rhythm-params []
       {:steps 24 :pulses (+ 3 (// state.height 8)) :accents 3 :offset (math.random 0 4) :len (* 2 tempo.bar)})
     (fn self.banger []
       (self.rhythm.replace (self.rhythm-params)))
     (for [i 1 4 1]
       (self.rhythm.set i (self.rhythm-params)))
     self)})

(fn music-scale []
  (let [t state.thresholds
        max-score (+ (* (length state.workers) 10)
                     (* (length state.shops) 2))
        worker-score
        (accumulate [sum 0
                     _ worker (ipairs state.workers)]
          (+ sum
             (if (< worker.hunger t.leave-hunger) 1 0)
             (if (< worker.hunger t.eat) 1 0)
             (if (< worker.fatigue t.leave-fatigue) 1 0)
             (if (< worker.fatigue t.rest) 1 0)
             (if (< worker.frustration t.leave-frustration) 1 0)
             (if (< worker.frustration t.study) 1 0)
             (if (< worker.queue-boredom t.boredom) 1 0)
             (if (< worker.injury t.heal) 3 0)))
        shop-score
        (accumulate [sum 0 _ shop (ipairs state.shops)]
          (+ sum (if shop.staff 2 0)))
        score (/ (+ worker-score shop-score) max-score)]
    (if (< 0.95 score) music-scales.pmaj
        (< 0.85 score) music-scales.bmaj
        (< 0.6 score) music-scales.susp
        (< 0.4 score) music-scales.pmin
        music-scales.dim)))

(fn music []
  (when (% tempo.tic tempo.lcm)
    (let [ws-ratio (/ state.height 4 (length state.workers))
          qscale (music-scale)]
      (play-voice voices.perc tempo.tic qscale ws-ratio)
      (play-voice voices.bass tempo.tic qscale ws-ratio)
      (play-voice voices.harm1 tempo.tic qscale ws-ratio)
      (play-voice voices.harm2 tempo.tic qscale ws-ratio)))
  (tempo.tick))

(fn TIC []
  (input)
  (when (not state.pause)
    (for [_ 1 state.speed]
      (update)))
  (draw)
  (music))

;;; init

(when (not (. _G :TIC))
  (for [i 1 4]
    (table.insert state.workers (make-worker (+ (* 8 i) (math.random 150))
                                             (- ground-level 1))))

  (say ["finally! a place untouched
by famine, plague, or flood"
        "we can build a refuge here,
a place for everyone to eat and
sleep!"
        "it's kinda small tho,
since we can't build out, so
we're going to have to build up"]
       :worker nil (. state.workers 1))
  (tset _G :TIC TIC))

;; <TILES>
;; 000:7777777777777777777777777777777777777777777777777777777777777777
;; 001:2222022222322222222223221222422222292221242222222222223292212222
;; 002:2232222329222212222422223222222222922322222222212232229220223222
;; 003:2221223222224222293222223222222222242223222220222229222221222232
;; 004:2322133222222331222222221334224223022222222233222222312221222222
;; 005:9099899099999999299922921394224223022222222233222222312221229222
;; 006:9099909999999919299499923222922222229322222222212932222220223222
;; 007:4455446455565555555555554555555545555555454555554545655592923299
;; 008:4455444455565454555655554555555555555555454556554545555592992939
;; 009:3433434366556666666666665666666766662566667222666632222662222223
;; 012:0300000302444443033233230300000303000003034444420233322303000003
;; 013:0000000308010103100080030010001380555553084446431045444308454443
;; 016:5665666667666766566666665656666756666656666656665667666655666666
;; 017:5666666656666766567666566666666656666646565666666766656656667666
;; 018:5676666656656665666666665566666656666656766656665666676656666766
;; 019:5566666666667666566666565666656656466666566666675676665676656666
;; 020:0999209944922994556666665666656756666666666566655566666656667666
;; 021:9909002942229999566566666666656656666676567666665666666565667666
;; 023:7777777776777677765766776557565745465456435544455444534444544443
;; 024:7777777777676677776656767655566565545466434445344454444534454344
;; 025:6222222372223222622222236222222262222224522222236232222262222223
;; 028:0100000000001071777706700660067154555555434534444343443453444435
;; 029:0700008307080003770080046660008355565553434543424444434344344443
;; 032:4554555457666664454555534666766435555454366666754555455446766664
;; 033:0000009980009999009999090099939909990999089999999990990999999999
;; 034:9909989999990990098999029990998293999023999093229099822209999232
;; 035:4344442499266655442444259226666655555555932666665244445592666676
;; 036:2334777756677777235665775555637756655377666665775555657766666377
;; 037:7744777777444777744444777733377777746777777333777743344777433447
;; 038:7775777757775776557557657567575555755675756757555567567575755755
;; 041:9089989922442224624665666656666667666666666665666667666665666666
;; 044:801008106abcdef66abcdef66abcdef656555554454444434344454344434344
;; 045:0000008310008083108080831088888355555553444345434543444345434433
;; 048:4555555436666764345555534667666435555554466666753555554346766654
;; 049:9999909990899999929909809899000880080000000608990906609299999922
;; 050:0990993200000993000999320000993208006923000663220089822299922322
;; 051:4445554599265666445555469266666644444555924666664254455599223756
;; 052:5555657766666577555525767666556655466464666655455556544466664434
;; 053:7743334776733347765333776557965745469456435594455444934444549443
;; 054:5577577575675675756656757555565565544455434445544454444534454344
;; 057:6666665666566766323333333334332343332334080080800010010010001001
;; 144:ffffffffffffffffffffffffffffffffffffff2fffffff2ffffff445fffff766
;; 145:ffffffffffffffffffffffffffffffffffff2fffffff2fff445444ff266666ff
;; 146:ffffffffffffffffffffffffffffffffffffffffff999ffff96269ff9662669f
;; 160:fffff626fffff626fffff766ffffffffffffffffffffffffffffffffffffffff
;; 161:226627ff666226ff666766ffffffffffffffffffffffffffffffffffffffffff
;; 162:9662269f9666669ff96669ffff999fffffffffffffffffffffffffffffffffff
;; 176:2822282265566656506660006016088860168811601681116016811160168111
;; 177:2228223255666565006506068806060618860606118600061186606711856066
;; 178:2282232255655255666666656676666656666676656656666666666666676666
;; 179:2228822255454454600800006066676650611816685666666061111660666656
;; 180:2822222255654466008080008676666016111168066666508611816086666560
;; 181:3282223866554645566666656676666666667666666666666011111160888888
;; 182:8228322366456446666566667666666666667066666660561111106788888066
;; 183:2282213256555546665666667666661066676610666611806666000066666718
;; 184:2212881344444564666766666666656666666666816676650866667666666666
;; 185:8224281456566566666656666666666766766666666676656665667666666666
;; 186:2282248243456436676666766656666666666667766676666666666666766566
;; 192:6016881160060888200340003003332329294333545280804442180133220100
;; 193:1886605688056076003990349433302339294933800800800100180180100180
;; 194:6566665666666667343344432324332392229292800880830081080381001013
;; 195:586181166067666620008000922223236abcdef66abcdef60800800801080100
;; 196:16181168866766600080000092323223abcdef70abcdef780800800810800010
;; 197:6000000060656676999999343433392333234333318111812100180130100100
;; 198:0000005666656076233293343433332399294333111881130100180900100102
;; 199:6766668066656666292329932233222333433343381188114008010830108080
;; 200:6566766666666666293349234223323343334334118811180180180118008108
;; 201:6766666666665667433334293233233442433223080080088010181018080188
;; 202:6566666766656676342323223332343232244233080800881080801801100801
;; 208:4542808032231181444280002343180044521554322210305443803023231040
;; 209:0008000011811811180080818000800555508405830083048405540384030304
;; 210:0080800200000002000000039966995355556552544344324344543243545443
;; 211:3223322311abf2e898999399dcb183a009929298f1ed128b899093991a1ec28d
;; 212:23320080c083808098928080fbe38000999354548ad2999289929299c0f39299
;; 213:2000000021811180200880003000808020844544300323323102221220030800
;; 214:0000000411181113808080038000300454443103323330032212308200803002
;; 215:2008080028000018300008103055008048025010300425552083343328029223
;; 216:0080008011188188020029010190100988001002555089814388100822008901
;; 217:8080800811811176008105668882856500014344818112920908023280091222
;; 218:0110011001107110000061106605600034434330222929002132320032223200
;; 224:ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
;; 225:fffffffffffffffffffffffffffffffffffffffffffffffff800080ff001108f
;; 227:ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
;; 228:fffffffffffffffffffffffffffffffffffffffffffffffff80080fff01100ff
;; 229:fffffffffffffffffffffffffffffffffffffffffffffffff7666666f7656766
;; 230:ffffffffffffffffffffffffffffffffffffffffffffffff7666676f7567667f
;; 231:fffffffffffffffffffffffffffffffffffffffffffffffff7667576f7656566
;; 232:ffffffffffffffffffffffffffffffffffffffffffffffff5767567f7756576f
;; 240:fffffff0ffffff71fffff670fffff760fffff554ffffffffffffffffffffffff
;; 241:0011110f0004998f7704490f630010ff553314ffff0113ffff1854ffff1803ff
;; 243:3223322381181288989923291181831009929298911882188930939918118281
;; 244:2111108018440080994465608815645094116564841199922455129984941299
;; 245:f7656756f6656656f6756656f6656756f6756656f6656655f6556645f5435634
;; 246:7567567f6567567f7566566f7567566f6566567f6566556f6556456f5345345f
;; 247:f7656676f6656656f5656756f5656756f6656757f6556656f6546755f5435434
;; 248:5756675f5656675f5656665f6655665767556757674566556645674557346534
;; </TILES>

;; <SPRITES>
;; 000:7888777773337777773777778000877777077777700077777070777788788777
;; 001:777777777777777777777777b777777b7b7777b7b777777b7777777777777777
;; 002:7777777777777777777777771777777171777717777777771177771177777777
;; 003:7771177777111177777399777773397777773777777777777777777777777777
;; 004:b77272777b727277b77222777b772777b7772777777777777777777777777777
;; 005:7777777777777777cc77227777722227cc72222777772277cc77777777777777
;; 009:0000000000000000000000000000000000002000000222000032222002222223
;; 010:000000000000000000000000000000000000a000000aaa0000aaaaa00aaaaaaa
;; 011:000000000000000000000000000000000000b000000bbb0000bbbbb00bbbbbbb
;; 012:000000000000000000000000000000000000c000000ccc0000ccccc00ccccccc
;; 013:000000000000000000000000000000000000d000000ddd0000dd0dd00ddddddd
;; 014:000000000000000000000000000000000000e000000eee0000eeeee00eeeeeee
;; 015:000000000000000000000000000000000000f000000fff0000fffff00fffffff
;; 016:ffffffffff9991fff929991ff999999ff081999fff0829fffff08fffffffffff
;; 017:7777777777777777777777777777777777777777d77777d76d77776dd77777d7
;; 018:77777777cc7777cc77777777cc7777cc77777777cc7777cc7777777777777777
;; 019:0777707770770777777772774444427722222277277772777777777777777777
;; 020:d7d7d7d77d7d7d77222222272663662726636627266366272222222777777777
;; 021:9999299723922227332334377777777777777777777777777777777777777777
;; 026:0aa0aaaa0aaaaaaa0aaaaaaa0aaaa0aa0aaaaaaa0aaaaaaa0a0aaaaa0aaaaaaa
;; 027:0bbbbbbb0b0bbbbb0bbbbbbb0bbbbbbb0bbbbbbb0bbbbbbb0bbbb0bb0bbbbbbb
;; 028:00cccccc0ccccccc0cccccc00ccccccc0ccccccc0ccccccc0ccc0ccc0ccccccc
;; 029:0ddddddd0ddddddd0ddddddd0d0ddddd0dddd0dd0ddddddd0ddddddd0ddddddd
;; 030:0eee0eee0eeeeeee0eeeeeee0e0eeeee0eeeeeee0eeeeeee0eeeeeee0eeee0ee
;; 031:0ff0ffff0fffffff0fffffff0ffff0ff0fffffff0fffffff0f0fffff0fffffff
;; 032:7771177777111177777399777773397777773777777777777777377777733777
;; 033:7771177777111177777399777773397777773777777777777777377777734777
;; 034:0000111000003330000033300000030000006660000000000000000000000000
;; 035:0707077707070777070007770770777707707777077077777777777777777777
;; 036:7777770711111107000000070777770777777777777777777777777777777777
;; 042:77777777777aaa7777aaaaa777aaaaa7777aaa7777777a777777a77777777777
;; 043:77777777777bbb7777bbbbb777bbbbb7777bbb7777777b777777b77777777777
;; 044:77777777777ccc7777ccccc777ccccc7777ccc7777777c777777c77777777777
;; 045:77777777777ddd7777ddddd777ddddd7777ddd7777777d777777d77777777777
;; 046:77777777777eee7777eeeee777eeeee7777eee7777777e777777e77777777777
;; 047:77777777777fff7777fffff777fffff7777fff7777777f777777f77777777777
;; 048:7333777777777777777007777777777777377777773327777732277777188777
;; 049:7333777777777777777007777777777777277777772237777723377777811777
;; 050:7777777775777777557777777577777777777777777777777777777777777777
;; 051:7700777777007777000000770000007777007777770077777777777777777777
;; 052:8888888786686687877877878668668788888887777777777777777777777777
;; 058:777777777777777777777777777777777777777777777777777aa777777aa777
;; 059:777777777777777777777777777777777777777777777777777bb777777bb777
;; 060:777777777777777777777777777777777777777777777777777cc777777cc777
;; 061:777777777777777777777777777777777777777777777777777dd777777dd777
;; 062:777777777777777777777777777777777777777777777777777ee777777ee777
;; 063:777777777777777777777777777777777777777777777777777ff777777ff777
;; 064:4445477777777777777777777777777777777777777777777777777777777777
;; 065:5555477777777777777777777777777777777777777777777777777777777777
;; 066:4344477777777777777777777777777777777777777777777777777777777777
;; 067:5555477777777777777777777777777777777777777777777777777777777777
;; 068:3445477777777777777777777777777777777777777777777777777777777777
;; 069:5455577777777777777777777777777777777777777777777777777777777777
;; 070:4443477777777777777777777777777777777777777777777777777777777777
;; 071:4555577777777777777777777777777777777777777777777777777777777777
;; 192:7777dddd7777777d7777777777eeee7b7777bbee777777bb777ddddd77ddeeee
;; 193:77777e77dd7bbe7b7dddbeebbbbdbbebebbbddddeeedbddbddbddbddeddeeddb
;; 194:77d777777dd777eeddbbbee7dbbeebbbdbeebbbdbdebddddeeedddeedddddee7
;; 195:77777777e7777777bb77777777777777dddddd77d7777777dd777777eed77777
;; 196:7777777777777777777777777777777777775775775565557756666677566666
;; 197:7777777777777777777777777777777755777777657557776656557766666577
;; 208:7777e79977777799777777967777779677777796777777967777779677777796
;; 209:999deebd9999ddbd6aa99dd9affa99d9affa69466aa666466666664666666646
;; 210:dd9999979999999799aa66979affa6979affa69766aa66976666669766666697
;; 211:77d7777777777777777777777777777777777777777777777777777777777777
;; 212:7555654575655554565556565545555555666665454545457444444577777744
;; 213:5556657755556577665545565666555555556556545454774444457747444477
;; 224:7777779677777796777777997777774974444449455555444445555445544555
;; 225:66666666666ccccc66ceeeee6ccccccc9c666666966666669966666644999999
;; 226:66666697cc666697ecc66997cccc699766669944664444446455555544555555
;; 227:7777777777777777777777777444777745554777555554775544547755644777
;; 228:7777777777777777777777777777777677777775777777767777777777777777
;; 229:7777777777777777777775776666555755666667666666674565545777677777
;; 240:7455565574546665744466667774666677745555777744667777744477777777
;; 241:5424444454445555666666666666665555566666666556664466664474444447
;; 242:4544444655665666566666666666555566666666666666664444444477777777
;; 243:6666547766666547666666645666655455555564666444474444777777777777
;; 244:7777457777655657774666557766666655455666466666666455454477547764
;; 245:7755477774565557566666576665665566466665455666555754654777755477
;; </SPRITES>

;; <MAP>
;; 123:000000000000020080708070000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 124:000000000000020121212100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 125:000000000000033111011100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 126:324200000000023101311111000000000000000000000000000000005262000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 127:334371817181037070807080808171817181718171817181718171815363718171817181718171817181718171817181718171817181718171817181000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 128:122250506060606060505050605050506050505050506050505050505060606050605060506050505050506060606060606060605050506060505050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 129:132320204040403030104030303030301020204040201010102020202030303040403010103030303030302020203040404040401010401030303030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 130:301030204010304010404010303010101010403010404010202040404020404010102020102020202030302030303030404030301030204010303030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 131:101010304010304040302040201010303040404020202040404030404030403010101010203040203020302030304040404040101010302010303030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 132:101010104010103030202040102010104040404040404020204040103030303010202030201010101020303010101010103040202020202040402020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 133:101010102030101020203010404040304040404040404020101010103030303010102040101020403010203010402020102020202040301010101030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 134:303010102010102010101030302010204040401040102040404040401030303030201010303030403010101040303030102020201010103020102020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 135:303030101020203030303010102020303030303040404040304040103030303030303030303030303030202020302020101010402040402040404020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; </MAP>

;; <WAVES>
;; 001:8aabbcbbaa9889abcdefffeea8420024
;; 002:47ceeffe8549ceeeda9abccba8654332
;; 003:17cefb5758cd6679a98ac75799896752
;; 004:ffedddccbaa988877776655544332110
;; 005:9aa8abcbdeffdcca9557422341112345
;; 006:6788abccdeeeedba7100234455566777
;; 008:89abcdeeffedcbba9865431000112344
;; </WAVES>

;; <SFX>
;; 000:7873184468518840a83fb812c801e80fe800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800102000000000
;; 001:746f128062a484a2a050b32ec10ee10fe00ef000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000104000000000
;; 002:730a142b624c835da58db1bfc0c1e0f6e0f7f0f7f0e7f0f0f0f0f0f0f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000115000000000
;; 004:0000200050009000d000e000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000529000000000
;; 008:01101110213021403150416051707190a1a0d1c0e1f0f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100584000000000
;; 009:02101210223022403250426052707290a2a0d2c0e2f0f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200586000000000
;; 010:03101310233023403350436053707390a3a0d3c0e3f0f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300586000000000
;; 011:04101410243024403450446054707490a4a0d4c0e4f0f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400586000000000
;; 012:05101510253025403550456055707590a5a0d5c0e5f0f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500586000000000
;; 013:06101610263026403650466056707690a6a0d6c0e6f0f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600586000000000
;; 016:d100910151012100010f110f310f410f61007101810191019101a100a10fb10fc100c100d100d100d100e100e100f100f100f100f100f100f100f100244000000000
;; 017:d200920152012200020f120f320f420f62007201820192019201a200a20fb20fc200c200d200d200d200e200e200f200f200f200f200f200f200f200244000000000
;; 018:d300930153012300030f130f330f430f63007301830193019301a300a30fb30fc300c300d300d300d300e300e300f300f300f300f300f300f300f300244000000000
;; 019:d400940154012400040f140f340f440f64007401840194019401a400a40fb40fc400c400d400d400d400e400e400f400f400f400f400f400f400f400244000000000
;; 020:d500950155012500050f150f350f450f65007501850195019501a500a50fb50fc500c500d500d500d500e500e500f500f500f500f500f500f500f500244000000000
;; 021:d600960156012600060f160f360f460f66007601860196019601a600a60fb60fc600c600d600d600d600e600e600f600f600f600f600f600f600f600244000000000
;; 024:01101110213021403150416051707190a1a0d1c0e1f0f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100584200000000
;; 025:02101210223022403250426052707290a2a0d2c0e2f0f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200585200000000
;; 026:03101310233023403350436053707390a3a0d3c0e3f0f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300586200000000
;; 027:04101410243024403450446054707490a4a0d4c0e4f0f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400586200000000
;; 028:05101510253025403550456055707590a5a0d5c0e5f0f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500586200000000
;; 029:06101610263026403650466056707690a6a0d6c0e6f0f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600586200000000
;; 032:d100910151012100010f110f310f410f61007101810191019101a100a10fb10fc100c100d100d100d100e100e100f100f100f100f100f100f100f100244200000000
;; 033:d200920152012200020f120f320f420f62007201820192019201a200a20fb20fc200c200d200d200d200e200e200f200f200f200f200f200f200f200244200000000
;; 034:d300930153012300030f130f330f430f63007301830193019301a300a30fb30fc300c300d300d300d300e300e300f300f300f300f300f300f300f300245200000000
;; 035:d400940154012400040f140f340f440f64007401840194019401a400a40fb40fc400c400d400d400d400e400e400f400f400f400f400f400f400f400244200000000
;; 036:d500950155012500050f150f350f450f65007501850195019501a500a50fb50fc500c500d500d500d500e500e500f500f500f500f500f500f500f500244200000000
;; 037:d600960156012600060f160f360f460f66007601860196019601a600a60fb60fc600c600d600d600d600e600e600f600f600f600f600f600f600f600244200000000
;; 040:01101110213021403150416051707190a1a0d1c0e1f0f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100584100000000
;; 041:02101210223022403250426052707290a2a0d2c0e2f0f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200586100000000
;; 042:03101310233023403350436053707390a3a0d3c0e3f0f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300586100000000
;; 043:04101410243024403450446054707490a4a0d4c0e4f0f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400586100000000
;; 044:05101510253025403550456055707590a5a0d5c0e5f0f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500586100000000
;; 045:06101610263026403650466056707690a6a0d6c0e6f0f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600586100000000
;; 048:d100910151012100010f110f310f410f61007101810191019101a100a10fb10fc100c100d100d100d100e100e100f100f100f100f100f100f100f100244100000000
;; 049:d200920152012200020f120f320f420f62007201820192019201a200a20fb20fc200c200d200d200d200e200e200f200f200f200f200f200f200f200244100000000
;; 050:d300930153012300030f130f330f430f63007301830193019301a300a30fb30fc300c300d300d300d300e300e300f300f300f300f300f300f300f300244100000000
;; 051:d400940154012400040f140f340f440f64007401840194019401a400a40fb40fc400c400d400d400d400e400e400f400f400f400f400f400f400f400244100000000
;; 052:d500950155012500050f150f350f450f65007501850195019501a500a50fb50fc500c500d500d500d500e500e500f500f500f500f500f500f500f500244100000000
;; 053:d600960156012600060f160f360f460f66007601860196019601a600a60fb60fc600c600d600d600d600e600e600f600f600f600f600f600f600f600244100000000
;; </SFX>

;; <TRACKS>
;; 000:100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; </TRACKS>

;; <PALETTE>
;; 000:1c1c344b4a6a7360528e7966ac9c83d4c6abe3e3ddf8f7f52b2b4a3c32281447ffcc13ffd0034ee7b601a5de0300d0ba
;; </PALETTE>

