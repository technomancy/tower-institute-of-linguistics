# The Tower Institute of Linguistics

You have these little characters running around. They have goals,
mostly centered around trying to build a tower to heaven. They quarry
and build, and then sometimes need to eat and sleep. The problem is
the god doesn't really like what you're doing and will periodically
cause a Babel Event that changes up the language of half your people.

* Enter: pause
* Space: choose what to build
* Tab: toggle speed
* Up/down: scroll
* Home/end: jump to top/bottom

Click on the quarry to boot out a single idle quarrier.

## Notes

Coordinates: x and y coordinates are pixel coordinates on the tic80
map grid; ground level is currently defined as 128 tiles down. (135 is
the lowest tile, so in theory we could go underground a little bit,
maybe for quarrying?)

Coordinates given in `tx`/`ty` are tile-level coordinates and need to
be multiplied by 8 to produce pixel coordinates, but `mset`/`mget`
take tile coordinates as args.

During development: 
* X: speed up time
* backspace: induce hunger in random worker

Colors:
* 0: black
* 1: purple
* 2: darker brown
* 3: lighter brown
* 4: beige
* 5: light beige
* 6: gray
* 7: white

* 8: dark purple
* 9: dark brown
* 10: blue
* 11: fuschia
* 12: red
* 13: orange/yellow
* 14: green
* 15: cyan

## TODO:

* is Blue Language English?
* if so, what are the other languages?
  * conlangs? 
    * then we need fonts for them, or deterministic mapping to latin words
  * real languages
    * then can we translate all worker dialog to all 5 of them?

* [x] Workers move around
* [x] Quarrying and building
* [x] Actually draw the built tiles
* [x] Stairs
* [x] Hunger
* [x] Build floors as the tower goes up
* [X] Intro text?
* [X] New workers arriving
* [X] Pause
* [X] Workers build floor on ground level
* [X] Coordination to quarry and build
* [X] Workers build 4-tile level from floor
* [X] Vertical movement allowed without stairs within floor
* [X] One worker per food-shop
* [X] Languages for coordination
* [X] Babel Incident
* [X] Staffing shops
* [X] Staffed shops show language color
* [X] Status shows language count
* [X] Language institutes (study)
* [X] Boot out single quarrier
* [X] Workers leaving from hunger
* [X] View worker details
* [X] Talking bubble shows language of collaboration
* [X] Fatigue/rest
* [X] The god's dialog at the top; workers at the bottom
* [X] Scrolling follows focused worker
* [X] Workers leaving from frustration/fatigue
* [X] Focused worker status shows current task
* [X] Cancel button for workers (queue/wait only)
* [X] Idle quarry workers with no partner eventually cancel
* [X] A long queue should look long
* [X] Climb fatigue
* [X] When climbing, periodically attempt to meet needs/follow orders/help
* [X] New workers arrive in "families" that sometimes include two languages
* [X] Food shop staff have to resupply periodically
* [X] Status display for # workers hungry / tired
* [X] Winning animation
* [X] Height meter
* [X] Should babel incidents allow bilingual workers to remain bilingual?
* [X] Injuries
* [X] Double-staff bug
* [X] Babel incident cancels staffing orders
* [X] Pause should either prevent dialogs or accept input in dialog
* [X] Click speech to dismiss it
* [X] You can unstaff a shop when the shopkeeper is supposedly busy
* [X] Workers build walls over shop tiles
* [X] Thresholds drop as tower gets higher?
* [X] Talk bubbles in speech box
* [X] Alignment of second bubble is off
* [X] Workers are drawn behind shop flags
* [-] Dialog explaining mechanics
* [ ] Orders should "steal" workers waiting in queue
* [ ] Stairs have floor tiles at floor level
* [X] Double-build bug
* [X] Hover tooltips
* [X] Don't wait indefinitely for help when building
* [X] Threshold that are exceeded should turn red in focus section
* [X] Scroll wheel?
* [X] Click target boxes aren't right (workers and shops)
* [X] Endgame: count how many workers left total
* [X] Button for easy mode
* [X] Visual indication that a shop has staff requested/coming?
* [X] Building, talk is set to pants language, not shared language

### Later-game
* [ ] Elevator? (when the tower gets very tall)
* [ ] Shop for processing rocks - makes _n_ builders more efficient
* [ ] Shop for processing food - makes _n_ food shops more efficient
* [ ] Tower must widen when it gets to a certain height?
* [ ] Ability to "destroy" a shop so you can replace it?

### Graphics

* [x] Tiles for clouds (at the top of the screen, and bg)
* [x] Sprites for the god
* [x] Sprites to indicate hunger
* [x] Sprites to indicate tiredness
* [x] Sprites indicating injury
* [x] Code drawing sprites for worker status
* [x] Tiles for farm / food area?
* [ ] Tiles for food processing shop
* [ ] Tiles for rock processing shop
* [ ] Tiles for elevator
* [x] Code indicating capacity usage for a shop
* [x] Sprites indicating which languages a worker speaks
* [x] Code to draw worker language sprites
* [x] Medic tileset

### Music

* [x] Euclidean rhythm generator
* [x] "Shift Register" style value sequencer
* [x] Percussive tones driven by quarry/building work
* [x] Bass voice is fairly constant 1 tone / bar
* [x] Harmony driven from game health
* [x] Tonal voices switch between sfx driven by seq'd values

## License

© 2024 Matthew Lyon and Phil Hagelberg
Released under the MIT/X11 license
