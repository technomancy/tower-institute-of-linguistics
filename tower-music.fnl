;; title:   Tower Institute of Linguistics Music
;; author:  Matthew Lyon <matthew@lyonheart.us>
;; desc:    extracted generative music engine from Tower Institute of Linguistics
;; site:    website link
;; license: MIT License (change this to your license of choice)
;; version: 0.1
;; script:  fennel
;; strict:  true

(local state {:languages 1 :height 1 :workers 4 :morale 5})

(local tempo
  (let [tpb 72
        self {:tic 0
              :entropy 0 :perc 0
              :beat tpb :bar (* tpb 4) :cycle (* tpb 4 24)
              :s16 (/ tpb 4) :t16 (/ tpb 6) :s8 (/ tpb 2) :t8 (/ tpb 3)
              :lcm (/ tpb 12)}]
    (fn self.tick [_]
      (set self.tic (-> self.tic (+ 1) (% self.cycle))))
    self))

(fn make-euclid-pattern [steps pulses offset]
  (var pile (- steps pulses))
  (local pat [])
  (when (< 0 steps)
    (for [s 0 (- steps 1) 1]
      (let [sidx (-> s (+ offset steps) (% steps))]
        (set pile (+ pile pulses))
        (when (>= pile steps)
          (set pile (- pile steps))
          (table.insert pat sidx)))))
  pat)

(fn make-sequence [len]
  (local seq [])
  (for [_ 1 len 1]
    (table.insert seq (math.random)))
  (let [self {: seq :pos 1 : len}]
    (fn self.length [_ n]
      (when (> n (length self.seq))
        (for [_ 1 (- n (length self.seq)) 1]
          (table.insert self.seq (math.random))))
      (set self.len n))
    (fn self.replace [_] (tset self.seq self.pos (math.random)))
    (fn self.shift [_]
      (let [this-val (. self.seq self.pos)]
        (set self.pos (-> self.pos (+ 1) (% self.len) (+ 1)))
        this-val))
    self))

(local music-scales
  {:pmaj [0 2 4 7 9]
   :bmaj [0 2 5 7 9]
   :susp [0 2 5 7 10]
   :pmin [0 3 5 7 10]
   :dim [0 3 6 9]})

;; 1/oct -> 12/oct TET
(fn quantize-note [val scale]
  (let [oct (math.floor val)
        scl-idx (-> val (or 0) (% 1) (* (length scale)) math.ceil)]
    ;; adding 4 at the end, the tonic is E, because I'm difficult like that
    (-> scale (. scl-idx) (or 0) (+ 4) (+ (* oct 12)))))

(fn make-trigger-seq [steps pulses offset len]
  (let [step-len (/ len steps)
        pat (collect [_ v (pairs (make-euclid-pattern steps pulses offset))]
              (values (* v step-len) true))
        self {: pat : len}]
    (fn self.step [t] (. self.pat (% t self.len)))
    self))

(fn make-rhythm [steps pulses accents offset len]
  (let [pulses (math.min pulses steps)
        steps (if (= 0 pulses) 0 steps)
        pulse-pts (make-euclid-pattern steps pulses offset)
        accent-pts (collect [_ v (pairs (make-euclid-pattern pulses accents 0))]
                     (values v 0))
        step-len (/ len steps)
        self {: len
              :pat (collect [idx v (pairs pulse-pts)]
                     (values (* v step-len)
                             {:vol (if (. accent-pts idx) 12 6)}))}]
    (fn self.step [t] (. self.pat (% t self.len)))
    self))

(fn make-rhythmbox [len]
  (let [self { :pos 1 :seq [] : len}]
    (fn self.current [] (. self.seq self.pos))
    (fn self.step [t] (let [r (. self.seq self.pos)] (r.step t)))
    (fn self.set [p rs]
      (let [new-rhythm (make-rhythm rs.steps rs.pulses rs.accents rs.offset rs.len)]
        (set new-rhythm.speed rs.speed)
        (tset self.seq p new-rhythm)
        new-rhythm))
    (fn self.replace [spd rs]
      (self.set self.pos spd rs))
    (fn self.shift [_]
      (let [this-rhythm (. self.seq self.pos)]
        (set self.pos (-> self.pos (+ 1) (% self.len) (+ 1)))
        this-rhythm))
    self))

(fn sfxLRrand [base]
  (case (math.random 1 3)
    1 base
    2 (+ base 16)
    3 (+ base 32)))

(fn play-voice [voice tic qscale]
  (when (and voice.pitchsched
             (= 0 (% tic voice.pitchchange.len))
             (< (math.random) 0.1)
             (< 0 tempo.entropy))
    (set tempo.entropy (- tempo.entropy 1))
    (voice.pitchsched))

  (when (and voice.banger
             (= 0 (% tic voice.rhythm.len)))
    (when voice.rhythm.shift
      (voice.rhythm.shift))
    (when (or voice.every-banger
              (and (< (math.random) 0.1)
                   (< 0 tempo.entropy)))
      (when (not voice.every-banger)
        (set tempo.entropy (- tempo.entropy 1)))
      (voice.banger)))

  (when (and voice.pitchchange (voice.pitchchange.step tic))
    (when (and voice.pitchseq
               (< (math.random) 0.05)
               (< 0 tempo.entropy))
      (set tempo.entropy (- tempo.entropy 1))
      (voice.pitchseq.replace))
    (set voice.pitch (voice.pitchseq.shift)))

  (case (voice.rhythm.step tic)
    {: vol} (sfx (voice.sfx state.languages)
                 (voice.notepitch qscale)
                 -1 voice.channel vol (or voice.rhythm.speed voice.speed))))

(local voices
  {:perc
   (let [self
         {:channel 0 :speed 0 :every-banger true}]
     (fn self.sfx [_] (math.random 0 2))
     (fn self.notepitch [qscale]
       (math.random 0 (-> state.height (/ 100) (* 2) math.floor (+ 0.5) (quantize-note qscale))))
     (fn self.banger []
       (if (= 0 tempo.perc)
           (set self.rhythm (make-rhythm 4 0 0 0 tempo.beat))
           (let [perc tempo.perc ;;(// tempo.perc 2)
                 steps (if (> perc 6) 6
                           (> (math.random) 0.5) 4 6)
                 pulses (math.min perc steps)
                 accents (math.random 1 (-> perc (- pulses) (math.min (// steps 2)) (math.max 1)))]
             (set tempo.perc (-> perc (- pulses (- accents 1)) (math.max 0) (// 2) math.floor))
             (set self.rhythm (make-rhythm steps pulses accents 0 tempo.beat)))))
     (self.banger)
     self)

   :bass
   (let [self
         {:channel 1 :speed -4
          :pitch 0
          :pitchseq (make-sequence 4)
          :pitchchange (make-trigger-seq 4 4 0 (* 4 tempo.bar))
          :rhythm (make-rhythmbox 4)
          :offset 0 :len (* 4 tempo.bar)}]
     (fn self.notepitch [qscale]
       (-> self.pitch (+ 1) (quantize-note qscale)))
     (fn self.sfx [langs] (+ 15 (math.random 1 (math.min 6 langs))))
     (fn self.rhythm-params []
       (let [tpl (> (math.random) 0.5)
             [steps pulses accents speed]
             (if (< state.height 8) [4 4 1 -4]
                 (< state.height 20) (if tpl [6 (math.random 4 6) 2 -4]
                                             [8 (math.random 4 7) (math.random 1 2) -4])
                 (< state.height 50) (if tpl [12 (math.random 6 8) 3 -3]
                                             [16 (math.random 6 12) (math.random 3 5) -3])
                 (if tpl [24 (math.random 6 18) (math.random 4 8) -2]
                         [32 (math.random 8 24) (math.random 4 10) -1]))]
         {: steps : pulses : accents : speed :offset self.offset :len self.len}))
     (fn self.banger [] (self.rhythm.replace (self.rhythm-params)))
     (for [i 1 4 1]
       (self.rhythm.set i (self.rhythm-params)))
     self)

   :harm1
   (let [self {:channel 2 :speed -2
               :pitch 0
               :pitchseq (make-sequence 8)
               :rhythm (make-rhythmbox 4)}]
     (fn self.notepitch [qscale]
       (-> self.pitch
           (* (math.min 4 state.languages))
           (+ 1.5)
           (quantize-note qscale)))
     (fn self.sfx [l] (+ (sfxLRrand 15) (math.random 1 (math.min 6 l))))
     (fn self.pitchsched []
       (let [newlen (math.random 4 (+ 6 (math.floor (/ state.height 10))))]
         (set self.pitchchange (make-trigger-seq 24 newlen 0 (* 2 tempo.bar)))))
     (self.pitchsched)
     (fn self.rhythm-params []
       (let [len (math.random 1 4)
             steps (if (> (math.random) 0.5) (* len 8) (* len 12))
             pulse-base (* 3 len)
             pulse-max (+ pulse-base (math.ceil (/ state.height 25)))
             pulses (math.random pulse-base pulse-max)
             accents (math.random 1 (- pulses 1))
             offset (math.random 0 len)]
         { : steps : pulses : accents : offset :len (* len tempo.bar)}))
     (fn self.banger []
         (self.rhythm.replace (self.rhythm-params)))
     (for [i 1 4 1]
       (self.rhythm.set i (self.rhythm-params)))
     self)

   :harm2
   (let [self {:channel 3 :speed -1 :pitch 0 :pitchseq (make-sequence 24)
               :pitchchange (make-trigger-seq 24 24 0 tempo.bar)
               :rhythm (make-rhythmbox 4)}]
     (fn self.notepitch [qscale]
       (-> self.pitch
           (* (math.random 0 (math.min 4 state.languages)))
           (+ 1)
           (quantize-note qscale)))
     (fn self.sfx [l] (+ (sfxLRrand 15) (math.random 1 (math.min 6 l))))
     (fn self.rhythm-params []
       {:steps 24 :pulses (+ 3 (// state.height 8)) :accents 3 :offset (math.random 0 4) :len (* 2 tempo.bar)})
     (fn self.banger []
       (self.rhythm.replace (self.rhythm-params)))
     (for [i 1 4 1]
       (self.rhythm.set i (self.rhythm-params)))
     self)})

(fn music-scale []
  (case state.morale
    1 music-scales.bmin
    2 music-scales.pmin
    3 music-scales.susp
    4 music-scales.bmaj
    5 music-scales.dim))
  
(fn music []
  (when (% tempo.tic tempo.lcm)
    (let [qscale (music-scale)]
      (play-voice voices.perc tempo.tic qscale)
      (play-voice voices.bass tempo.tic qscale)
      (play-voice voices.harm1 tempo.tic qscale)
      (play-voice voices.harm2 tempo.tic qscale)))
  (tempo.tick))


(local activity-rates
       [{:display "none" :lower 0 :upper 0}
        {:display "low" :lower 0 :upper 2}
        {:display "med" :lower 1 :upper 4}
        {:display "high" :lower 2 :upper 6}
        {:display "max" :lower 4 :upper 6}])

(local entropy-rates
       [{:display "none" :lower 0 :upper 0}
        {:display "low" :lower 0 :upper 1}
        {:display "med" :lower 1 :upper 3}
        {:display "high" :lower 2 :upper 5}
        {:display "max" :lower 3 :upper 8}])

(local heights
       [{:display "short" :h 7}
        {:display "med" :h 19}
        {:display "big" :h 49}
        {:display "tall" :h 80}])

(local morales
       [{:display "bleak"}
        {:display "down"}
        {:display "meh"}
        {:display "good"}
        {:display "great"}])

(local
 gen-state
 {:var-sel 0
  :activity-rate 1
  :entropy-rate 1
  :height 1
  :morale 5
  :perc-history []
  :entropy-history []})

(fn update-sel [dir]
  (set gen-state.var-sel (-> gen-state.var-sel (+ dir) (% 4))))

(fn constrain-choice [k l change]
  (let [n (+ change (. gen-state k))]
    (if
      (< n 1) l
      (< l n) 1
      n)))

(fn update-selected [dir]
  (let [[k l] (case gen-state.var-sel
               0 [:activity-rate (length activity-rates)]
               1 [:entropy-rate (length entropy-rates)]
               2 [:height (length heights)]
               3 [:morale (length morales)])]
    (tset gen-state k (constrain-choice k l dir))
    (if (= 2 gen-state.var-sel)
        (let [{: h} (. heights gen-state.height)]
          (set state.languages (-> h (// 16) (math.max 6)))
          (set state.height h)))))

(fn input []
  (when (keyp 58) (update-sel -1))
  (when (keyp 59) (update-sel 1))
  (when (keyp 60) (update-selected -1))
  (when (keyp 61) (update-selected 1)))

(fn sel-color [n] (if (= gen-state.var-sel n) 15 7))

(fn draw []
  (cls 0)
  (print "activity rate" 12 10 (sel-color 0))
  (print (. (. activity-rates gen-state.activity-rate) :display) 130 10 (sel-color 0))
  (each [i n (ipairs gen-state.perc-history)]
    (rect (+ 180 i) (- 15 n) 1 n 7))
  (print "amount of percussion" 24 20 2)
  (print "entropy rate" 12 30 (sel-color 1))
  (print (. (. entropy-rates gen-state.entropy-rate) :display) 130 30 (sel-color 1))
  (each [i n (ipairs gen-state.entropy-history)]
    (rect (+ 180 i) (- 35 n) 1 n 7))
  (print "amount of change" 24 40 2)
  (print "tower height" 12 50 (sel-color 2))
  (print (. (. heights gen-state.height) :display) 130 50 (sel-color 2))
  (print "rhythm intensity, pitch range, voices" 24 60 2)
  (print "worker morale" 12 70 (sel-color 3))
  (print (. (. morales gen-state.morale) :display) 130 70 (sel-color 3))
  (print "scale of tones" 24 80 2)
  (let [sel-base (+ 9 (* gen-state.var-sel 20))]
   (tri 5 sel-base 5 (+ sel-base 7) 9 (+ sel-base 4) 15))
  (print "Tower Institute of Linguistics OST" 12 100 1))

(fn update []
  (when (= 0 (% tempo.tic tempo.beat))
    (let [{: lower : upper} (. activity-rates gen-state.activity-rate)]
      (set tempo.perc (+ tempo.perc (math.random lower upper)))))
  (when (= 0 (% tempo.tic tempo.bar))
    (let [{: lower : upper} (. activity-rates gen-state.entropy-rate)]
      (set tempo.entropy (+ tempo.entropy (math.random lower upper)))))
  (when (= 0 (% tempo.tic tempo.beat))
    (table.insert gen-state.perc-history 1 tempo.perc)
    (while (< 40 (length gen-state.perc-history))
      (table.remove gen-state.perc-history 41))
    (table.insert gen-state.entropy-history 1 tempo.entropy)
    (while (< 40 (length gen-state.entropy-history))
      (table.remove gen-state.entropy-history 41))))


(fn TIC []
  (input)
  (draw)
  (music)
  (update))

(when (not (. _G :TIC))
  (tset _G :TIC TIC))

;; <TILES>
;; 001:eccccccccc888888caaaaaaaca888888cacccccccacc0ccccacc0ccccacc0ccc
;; 002:ccccceee8888cceeaaaa0cee888a0ceeccca0ccc0cca0c0c0cca0c0c0cca0c0c
;; 003:eccccccccc888888caaaaaaaca888888cacccccccacccccccacc0ccccacc0ccc
;; 004:ccccceee8888cceeaaaa0cee888a0ceeccca0cccccca0c0c0cca0c0c0cca0c0c
;; 017:cacccccccaaaaaaacaaacaaacaaaaccccaaaaaaac8888888cc000cccecccccec
;; 018:ccca00ccaaaa0ccecaaa0ceeaaaa0ceeaaaa0cee8888ccee000cceeecccceeee
;; 019:cacccccccaaaaaaacaaacaaacaaaaccccaaaaaaac8888888cc000cccecccccec
;; 020:ccca00ccaaaa0ccecaaa0ceeaaaa0ceeaaaa0cee8888ccee000cceeecccceeee
;; </TILES>

;; <WAVES>
;; 001:8aabbcbbaa9889abcdefffeea8420024
;; 002:47ceeffe8549ceeeda9abccba8654332
;; 003:17cefb5758cd6679a98ac75799896752
;; 004:ffedddccbaa988877776655544332110
;; 005:9aa8abcbdeffdcca9557422341112345
;; 006:6788abccdeeeedba7100234455566777
;; 008:89abcdeeffedcbba9865431000112344
;; </WAVES>

;; <SFX>
;; 000:7873184468518840a83fb812c801e80fe800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800f800102000000000
;; 001:746f128062a484a2a050b32ec10ee10fe00ef000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000104000000000
;; 002:730a142b624c835da58db1bfc0c1e0f6e0f7f0f7f0e7f0f0f0f0f0f0f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000115000000000
;; 004:0000200050009000d000e000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000529000000000
;; 008:01101110213021403150416051707190a1a0d1c0e1f0f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100584000000000
;; 009:02101210223022403250426052707290a2a0d2c0e2f0f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200586000000000
;; 010:03101310233023403350436053707390a3a0d3c0e3f0f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300586000000000
;; 011:04101410243024403450446054707490a4a0d4c0e4f0f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400586000000000
;; 012:05101510253025403550456055707590a5a0d5c0e5f0f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500586000000000
;; 013:06101610263026403650466056707690a6a0d6c0e6f0f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600586000000000
;; 016:d100910151012100010f110f310f410f61007101810191019101a100a10fb10fc100c100d100d100d100e100e100f100f100f100f100f100f100f100244000000000
;; 017:d200920152012200020f120f320f420f62007201820192019201a200a20fb20fc200c200d200d200d200e200e200f200f200f200f200f200f200f200244000000000
;; 018:d300930153012300030f130f330f430f63007301830193019301a300a30fb30fc300c300d300d300d300e300e300f300f300f300f300f300f300f300244000000000
;; 019:d400940154012400040f140f340f440f64007401840194019401a400a40fb40fc400c400d400d400d400e400e400f400f400f400f400f400f400f400244000000000
;; 020:d500950155012500050f150f350f450f65007501850195019501a500a50fb50fc500c500d500d500d500e500e500f500f500f500f500f500f500f500244000000000
;; 021:d600960156012600060f160f360f460f66007601860196019601a600a60fb60fc600c600d600d600d600e600e600f600f600f600f600f600f600f600244000000000
;; 024:01101110213021403150416051707190a1a0d1c0e1f0f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100584200000000
;; 025:02101210223022403250426052707290a2a0d2c0e2f0f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200585200000000
;; 026:03101310233023403350436053707390a3a0d3c0e3f0f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300586200000000
;; 027:04101410243024403450446054707490a4a0d4c0e4f0f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400586200000000
;; 028:05101510253025403550456055707590a5a0d5c0e5f0f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500586200000000
;; 029:06101610263026403650466056707690a6a0d6c0e6f0f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600586200000000
;; 032:d100910151012100010f110f310f410f61007101810191019101a100a10fb10fc100c100d100d100d100e100e100f100f100f100f100f100f100f100244200000000
;; 033:d200920152012200020f120f320f420f62007201820192019201a200a20fb20fc200c200d200d200d200e200e200f200f200f200f200f200f200f200244200000000
;; 034:d300930153012300030f130f330f430f63007301830193019301a300a30fb30fc300c300d300d300d300e300e300f300f300f300f300f300f300f300245200000000
;; 035:d400940154012400040f140f340f440f64007401840194019401a400a40fb40fc400c400d400d400d400e400e400f400f400f400f400f400f400f400244200000000
;; 036:d500950155012500050f150f350f450f65007501850195019501a500a50fb50fc500c500d500d500d500e500e500f500f500f500f500f500f500f500244200000000
;; 037:d600960156012600060f160f360f460f66007601860196019601a600a60fb60fc600c600d600d600d600e600e600f600f600f600f600f600f600f600244200000000
;; 040:01101110213021403150416051707190a1a0d1c0e1f0f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100f100584100000000
;; 041:02101210223022403250426052707290a2a0d2c0e2f0f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200f200586100000000
;; 042:03101310233023403350436053707390a3a0d3c0e3f0f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300f300586100000000
;; 043:04101410243024403450446054707490a4a0d4c0e4f0f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400586100000000
;; 044:05101510253025403550456055707590a5a0d5c0e5f0f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500586100000000
;; 045:06101610263026403650466056707690a6a0d6c0e6f0f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600f600586100000000
;; 048:d100910151012100010f110f310f410f61007101810191019101a100a10fb10fc100c100d100d100d100e100e100f100f100f100f100f100f100f100244100000000
;; 049:d200920152012200020f120f320f420f62007201820192019201a200a20fb20fc200c200d200d200d200e200e200f200f200f200f200f200f200f200244100000000
;; 050:d300930153012300030f130f330f430f63007301830193019301a300a30fb30fc300c300d300d300d300e300e300f300f300f300f300f300f300f300244100000000
;; 051:d400940154012400040f140f340f440f64007401840194019401a400a40fb40fc400c400d400d400d400e400e400f400f400f400f400f400f400f400244100000000
;; 052:d500950155012500050f150f350f450f65007501850195019501a500a50fb50fc500c500d500d500d500e500e500f500f500f500f500f500f500f500244100000000
;; 053:d600960156012600060f160f360f460f66007601860196019601a600a60fb60fc600c600d600d600d600e600e600f600f600f600f600f600f600f600244100000000
;; </SFX>

;; <TRACKS>
;; 000:100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; </TRACKS>

;; <PALETTE>
;; 000:1c1c344b4a6a7360528e7966ac9c83d4c6abe3e3ddf8f7f52b2b4a3c32281447ffcc13ffd0034ee7b601a5de0300d0ba
;; </PALETTE>
