TIC_HOME ?= ~/.local/share/com.nesbox.tic/TIC-80/
TIC ?= tic80
run: ; $(TIC) tower.fnl

test: ; fennel --lua lua5.3 test.fnl tower.fnl

tower.html.zip: tower.fnl
	cp tower.fnl $(TIC_HOME)
	rm -f $(TIC_HOME)/tower.html.zip
	$(TIC) --cli --cmd "load tower.fnl & export html tower.html & exit"
	mv $(TIC_HOME)/tower.html.zip .

tower-ost.html.zip: tower-music.fnl
	cp tower-music.fnl $(TIC_HOME)
	rm -f $(TIC_HOME)/tower-music.html.zip
	$(TIC) --cli --cmd "load tower-music.fnl & export html tower-ost.html & exit"
    mv $(TIC_HOME)/tower-ost.html.zip .

upload: tower.html.zip
	butler push $< technomancy/tower-institute-of-linguistics:tower.html.zip

upload-draft: tower.html.zip
	rm -f draft/*
	unzip $< -d draft
	rsync -rAv draft/* p:p/tower-institute-of-linguistics/
	echo https://p.hagelb.org/tower-institute-of-linguistics/

count: ; cloc tower.fnl

check: ; fennel-ls --check tower.fnl

.PHONY: run test count upload upload-draft check
